#include "WarningYouUsedAllIngredients.h"
#include "ui_WarningYouUsedAllIngredients.h"

#include "MainWindow.h"
#include "IngredienteView.h"

WarningYouUsedAllIngredients::WarningYouUsedAllIngredients(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WarningYouUsedAllIngredients)
{
    ui->setupUi(this);
}

WarningYouUsedAllIngredients::~WarningYouUsedAllIngredients()
{
    delete ui;
}

void WarningYouUsedAllIngredients::on_WarningYouUsedAllIngredients_finished(int result)
{
    if (!result)
    {
        this->close();
        this->deleteLater();
    }
}

void WarningYouUsedAllIngredients::on_pushButton_clicked()
{
    this->close();
    this->deleteLater();
}

void WarningYouUsedAllIngredients::on_pushButton_2_clicked()
{

    this->close();
    this->deleteLater();
    MainWindow::m_Instance->on_actionIngrediente_triggered();
}
