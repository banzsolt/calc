#include "ReteteViewComboDelegate.h"
#include <QComboBox>
#include "ReteteView.h"
#include "ui_ReteteView.h"

ReteteViewComboDelegate::ReteteViewComboDelegate(QList<Ingredient *> itemList, QObject *parent): QItemDelegate(parent)
{
    items = itemList;
}

QWidget *ReteteViewComboDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex & index) const
{
     QComboBox *editor = new QComboBox(parent);
     QStringList list ;
     list << items.value(index.row())->GetType();
     editor->addItems(list);
     return editor;
}

void ReteteViewComboDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
     QString value = index.model()->data(index, Qt::DisplayRole).toString();
     QComboBox *comboBox = static_cast<QComboBox*>(editor);
     comboBox->addItem(value);
}

void ReteteViewComboDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
     QComboBox *comboBox = static_cast<QComboBox*>(editor);
     QString value = comboBox->currentText();
     model->setData(index, value, Qt::UserRole);
}

void ReteteViewComboDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}
