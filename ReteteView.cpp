#include "ReteteView.h"
#include "ui_ReteteView.h"

#include "ReteteViewListModel.h"


#include <qlistview.h>
#include <qstringlistmodel.h>
#include <qstandarditemmodel.h>
#include <qcombobox.h>
#include <qtreeview.h>
#include <qtableview.h>
#include <qcolor.h>
#include <QMouseEvent>
#include <QMenu>
#include <QEvent>


#include "Backend/ingredient.h"
#include "ReteteViewComboDelegate.h"
#include "ReteteViewComboDelegateIngredient.h"
#include "ReteteViewComboDelegateSemipreparat.h"
#include "RenamReteta.h"
#include "WarningForDeleteRetete.h"
#include "WarningYouUsedAllIngredients.h"
#include "QMessageBox"

#include "MainWindow.h"

ReteteView* ReteteView::m_Instance = 0;

ReteteView::ReteteView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReteteView)
{
    ui->setupUi(this);

    items.clear();
    itemsSemipreparate.clear();
    QString printable;


    m_Instance = this;

    currentSelectedRow = 0;
    currentIngredienteSelectedRow = 0;

    for (int i = 0 ; i < MainWindow::m_Instance->listaRetete->count(); ++i)
    {
        MainWindow::m_Instance->listaRetete->at(i)->Update();
        if (MainWindow::m_Instance->listaRetete->at(i)->pret >= MainWindow::m_Instance->listaRetete->at(i)->pretDeVanzare)
        {
            int reply;
            reply = QMessageBox::critical(this, "Fara profit",
                                 "Pretul de productie este mai mare decat cea de vanzare " + MainWindow::m_Instance->listaRetete->at(i)->name,
                                 "Am Inteles" ,"Ups o sa-l modific","         OK         ",2, -1);
        }
    }

    ui->pretDeFabricare->setReadOnly(true);

    MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->Update();

    ui->pretDeVanzare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pretDeVanzare));
    ui->pretDeFabricare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pret));
    ui->unitateDeMasura->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->unitateDeMasura));

    for (int i = 0; i < MainWindow::m_Instance->listaRetete->count(); ++i)
    {
        Reteta *newReteta = new Reteta();
        newReteta = MainWindow::m_Instance->listaRetete->at(i);
        ui->listaRetete->addItem(newReteta->name);

        QMap<Reteta*, int>::iterator it_2;
        QMap<Ingredient*, int>::iterator it;
        it = newReteta->ingrediente.begin();
        it_2 = newReteta->semipreparate.begin();

        for (int j =0; j < newReteta->ingrediente.count(); ++j)
        {
            Ingredient *ingredient = new Ingredient();

            for (int k = 0; k < MainWindow::m_Instance->listaIngred->size(); ++k)
            {
                Ingredient *newIngredient = new Ingredient();
                newIngredient = MainWindow::m_Instance->listaIngred->at(k);
                if (it.key()->GetName() == newIngredient->GetName())
                {
                    ingredient = MainWindow::m_Instance->listaIngred->at(k);
                    break;
                }
            }

            items.push_back(ingredient);
            it++;
        }

        for (int j =0; j < newReteta->semipreparate.count(); ++j)
        {
            Reteta *ingredient = new Reteta();

            for (int k = 0; k < MainWindow::m_Instance->listaRetete->size(); ++k)
            {
                Reteta *newReteta = new Reteta();
                newReteta = MainWindow::m_Instance->listaRetete->at(k);
                if (it_2.key()->name == newReteta->name)
                {
                    ingredient = MainWindow::m_Instance->listaRetete->at(k);
                    break;
                }
            }

            itemsSemipreparate.push_back(ingredient);
            it_2++;
        }
    }


    ReteteViewComboDelegateIngredient *delegate_1 = new ReteteViewComboDelegateIngredient(*MainWindow::m_Instance->listaIngred);
    ReteteViewComboDelegateSemipreparat *delegate_2 = new ReteteViewComboDelegateSemipreparat(*MainWindow::m_Instance->listaRetete);
    ui->detaliiRetete->setItemDelegateForColumn(0, delegate_1);
    ui->detaliiSemipreparat->setItemDelegateForColumn(0, delegate_2);


    ui->detaliiRetete->setFocusPolicy(Qt::NoFocus);
    ui->detaliiSemipreparat->setFocusPolicy(Qt::NoFocus);



    modelReteteView = new ReteteViewListModel(items);

    modelSemipreparatView = new ReteteViewSemiModel(itemsSemipreparate);

    ui->detaliiRetete->setModel(modelReteteView);
    ui->detaliiSemipreparat->setModel(modelSemipreparatView);

    ui->detaliiRetete->header()->resizeSection(0, 300);
    ui->detaliiSemipreparat->header()->resizeSection(0, 300);
}

ReteteView::~ReteteView()
{
    delete ui;
}

void ReteteView::contextMenuEvent(QContextMenuEvent *event)
{
    if ((event->pos().x() > 10) &&
        (event->pos().x() < (this->ui->detaliiRetete->height() - 5)) &&
        (event->pos().y() > 5) &&
        (event->pos().y() < (18 * MainWindow::m_Instance->listaRetete->count())) &&
        (event->pos().y() < (this->ui->detaliiRetete->width() - 5) )
       )
    {
        QMenu menu(this);
        QAction *editAction = new QAction(tr("Edit"), this);
        QAction *deleteAction = new QAction(tr("Delete"), this);


        menu.addAction(editAction);
        menu.addAction(deleteAction);

        connect(editAction, SIGNAL(triggered()), this, SLOT(editReteta()));
        connect(deleteAction, SIGNAL(triggered()), this, SLOT(deleteReteta()));

        menu.exec(event->globalPos());
    }
}

void ReteteView::reset()
{
    modelReteteView->ingredienteList.clear();
    modelReteteView->ingredienteList = items;
}

void ReteteView::on_listaRetete_itemSelectionChanged()
{
    reset();

    currentSelectedReteta = ui->listaRetete->currentItem()->text();
    currentSelectedRow = ui->listaRetete->currentRow();
    QString printable;

    if (MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pret >= MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pretDeVanzare)
    {
        MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->Update();
        int reply;
        reply = QMessageBox::critical(this, "Fara profit",
                             "Pretul de productie este mai mare decat cea de vanzare " + MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->name,
                             "Am Inteles" ,"Ups o sa-l modific","         OK         ",2, -1);
    }


    MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->Update();
    ui->pretDeVanzare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pretDeVanzare));
    ui->pretDeFabricare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pret));
    ui->unitateDeMasura->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->unitateDeMasura));

    items.clear();
    itemsSemipreparate.clear();

    Reteta *newReteta = new Reteta();
    newReteta = MainWindow::m_Instance->listaRetete->at(ui->listaRetete->currentRow());
    QMap<Ingredient*, int>::iterator it;
    QMap<Reteta*, int>::iterator it_2;
    it = newReteta->ingrediente.begin();
    it_2 = newReteta->semipreparate.begin();

    for (int j = 0; j < newReteta->ingrediente.count(); ++j)
    {
        Ingredient *ingredient = new Ingredient();
        ingredient = it.key();

        items.append(ingredient);
        it++;
    }
    for (int j = 0; j < newReteta->semipreparate.count(); ++j)
    {
        Reteta *reteta = new Reteta();
        reteta = it_2.key();

        itemsSemipreparate.append(reteta);
        it_2++;
    }

    ReteteViewComboDelegate *delegate = new ReteteViewComboDelegate(items);
    ui->detaliiRetete->setItemDelegateForColumn(1, delegate);

    ui->detaliiRetete->setFocusPolicy(Qt::NoFocus);
    ui->detaliiSemipreparat->setFocusPolicy(Qt::NoFocus);

    modelReteteView = new ReteteViewListModel(items);
    modelSemipreparatView = new ReteteViewSemiModel(itemsSemipreparate);

    ui->detaliiRetete->setModel(modelReteteView);
    ui->detaliiSemipreparat->setModel(modelSemipreparatView);

    ui->detaliiRetete->header()->resizeSection(0, 300);
    ui->detaliiSemipreparat->header()->resizeSection(0, 300);

    ui->detaliiSemipreparat->reset();
}



void ReteteView::on_addReteta_clicked()
{
    ui->listaRetete->addItem("Reteta noua");
    Reteta *retetaNoua = new Reteta("Reteta Noua", false ,0, 0);

    MainWindow::m_Instance->listaRetete->push_back(retetaNoua);

    MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);



}

void ReteteView::on_addIngredient_clicked()
{
    if(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->ingrediente.count() < MainWindow::m_Instance->listaIngred->count())
    {   
        modelReteteView->insertRows(modelReteteView->rowCount(), 1);

        QString printable;
        MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->Update();
        ui->pretDeVanzare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pretDeVanzare));
        ui->pretDeFabricare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pret));
        ui->unitateDeMasura->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->unitateDeMasura));
    }
    else
    {
        WarningYouUsedAllIngredients *warning = new WarningYouUsedAllIngredients;
        warning->exec();
    }
}


void ReteteView::on_listaRetete_clicked(const QModelIndex &index)
{
}

void ReteteView::on_addSemipreparat_clicked()
{
    MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

    modelSemipreparatView->insertRows(items.count() + 1, 1);

    QString printable;
    MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->Update();
    ui->pretDeVanzare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pretDeVanzare));
    ui->pretDeFabricare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pret));
    ui->unitateDeMasura->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->unitateDeMasura));
}

void ReteteView::on_detaliiSemipreparat_clicked(const QModelIndex &index)
{
    currentSemipreparatSelectedRow = index.row();
}



void ReteteView::on_detaliiRetete_clicked(const QModelIndex &index)
{
    currentIngredienteSelectedRow = index.row();
}

void ReteteView::editReteta()
{
    RenamReteta *newRenameReteta = new RenamReteta();
    newRenameReteta->exec();
}

void ReteteView::deleteReteta()
{
    WarningForDeleteRetete *warning = new WarningForDeleteRetete();
    warning->exec();
}

void ReteteView::on_removeIngredient_clicked()
{
    if (items.count() > 0)
    {
        int reply;
        reply = QMessageBox::warning(this, "Sterge ingredient",
                             "Doriti sa stergeti ingredientul: " + items.at(currentIngredienteSelectedRow)->name,
                             "Da" ,"Ups","         Nu         ",2, -1);
        if (reply == 0)
        {
            modelReteteView->removeRow(currentIngredienteSelectedRow);
            MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

            QString printable;
            MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->Update();
            ui->pretDeVanzare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pretDeVanzare));
            ui->pretDeFabricare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pret));
            ui->unitateDeMasura->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->unitateDeMasura));
        }

    }
}

void ReteteView::on_removeSemipreparat_clicked()
{
    if (itemsSemipreparate.count() > 0)
    {
        int reply;
        reply = QMessageBox::warning(this, "Sterge semipreparatul",
                             "Doriti sa stergeti semipreparatul: " + items.at(currentSemipreparatSelectedRow)->name,
                             "Da" ,"Ups","         Nu         ",2, -1); //QMessageBox::No);
        if (reply == 0)
        {
            modelSemipreparatView->removeRow(currentSemipreparatSelectedRow);
            MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

            QString printable;
            MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->Update();
            ui->pretDeVanzare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pretDeVanzare));
            ui->pretDeFabricare->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pret));
            ui->unitateDeMasura->setText(printable.number(MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->unitateDeMasura));
        }

    }
}

void ReteteView::on_pretDeVanzare_textChanged(const QString &arg1)
{
    MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->pretDeVanzare = arg1.toDouble();
    MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);
}

void ReteteView::on_unitateDeMasura_textChanged(const QString &arg1)
{
    MainWindow::m_Instance->listaRetete->at(currentSelectedRow)->unitateDeMasura = arg1.toInt();
    MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);
}
