#ifndef INGREDIENTEVIEW_H
#define INGREDIENTEVIEW_H

#include <QWidget>
#include <QListWidgetItem>

namespace Ui {
class IngredienteView;
}

class IngredienteView : public QWidget
{
    Q_OBJECT

public:
    explicit IngredienteView(QWidget *parent = 0);
    ~IngredienteView();

    QString nameOfEditedItem;
    int indexOfEditedItem;

    int currentSelectedIngredient;

private slots:
    void on_adaugareIngredient_clicked();

    void on_numeIngredientTextbox_textChanged(const QString &arg1);
    void on_listaIngredients_itemClicked(QListWidgetItem *item);

    void on_pretTextbox_textChanged(const QString &arg1);

    void on_unitateTextbox_textChanged(const QString &arg1);

    void on_removeIngredient_clicked();

    void on_listaIngredients_clicked(const QModelIndex &index);

private:
    Ui::IngredienteView *ui;
};

#endif // INGREDIENTEVIEW_H
