#-------------------------------------------------
#
# Project created by QtCreator 2014-01-25T09:51:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Calc
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    ReteteView.cpp \
    IngredienteView.cpp \
    VanzariView.cpp \
    RaportView.cpp \
    SQDMultiColumnListWidget.cpp \
    ReteteViewListModel.cpp \
    Backend/listingredient.cpp \
    Backend/Ingredient.cpp \
    ReteteViewComboDelegate.cpp \
    Backend/ReadFromFile.cpp \
    Backend/ListReteta.cpp \
    VanzariViewListModel.cpp \
    Backend/Reteta.cpp \
    ReteteViewSemiModel.cpp \
    ReteteViewComboDelegateIngredient.cpp \
    ReteteViewComboDelegateSemipreparat.cpp \
    Backend/ChestiiVandute.cpp \
    RenamReteta.cpp \
    WarningForDeleteRetete.cpp \
    WarningYouUsedAllIngredients.cpp

HEADERS  += MainWindow.h \
    ReteteView.h \
    IngredienteView.h \
    VanzariView.h \
    RaportView.h \
    SQDMultiColumnListWidget.h \
    ReteteViewListModel.h \
    Backend/listingredient.h \
    Backend/Ingredient.h \
    ReteteViewComboDelegate.h \
    Backend/ReadFromFile.h \
    Backend/ListReteta.h \
    VanzariViewListModel.h \
    Backend/Reteta.h \
    ReteteViewSemiModel.h \
    ReteteViewComboDelegateIngredient.h \
    ReteteViewComboDelegateSemipreparat.h \
    Backend/ChestiiVandute.h \
    RenamReteta.h \
    WarningForDeleteRetete.h \
    WarningYouUsedAllIngredients.h

FORMS    += MainWindow.ui \
    ReteteView.ui \
    IngredienteView.ui \
    VanzariView.ui \
    RaportView.ui \
    RenamReteta.ui \
    WarningForDeleteRetete.ui \
    WarningYouUsedAllIngredients.ui
