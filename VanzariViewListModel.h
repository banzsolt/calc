#ifndef VANZARIVIEWLISTMODEL_H
#define VANZARIVIEWLISTMODEL_H

#include <QAbstractListModel>
#include "Backend/reteta.h"
#include "Backend/ChestiiVandute.h"
#include "QMutex"

class VanzariViewListModel : public QAbstractListModel
{
    Q_OBJECT
public:



    VanzariViewListModel(QObject *parent = 0);
    VanzariViewListModel(QList<Reteta*> ingrediente, QObject *parent = 0);
    ~VanzariViewListModel();

    enum Roles {ComboRole = Qt::UserRole};

    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int selection, Qt::Orientation orientation, int role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent) const;

    void setComboData(int row, QString name);

    bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex());
    bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex());
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    QList<Reteta*> reteteList;





signals:

public slots:

};

#endif // VANZARIVIEWLISTMODEL_H
