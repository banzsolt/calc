#ifndef RETETEVIEWCOMBODELEGATEINGREDIENT_H
#define RETETEVIEWCOMBODELEGATEINGREDIENT_H

#include <QItemDelegate>
#include "Backend/ingredient.h"

class ReteteViewComboDelegateIngredient : public QItemDelegate
{
    Q_OBJECT
public:
    //explicit ReteteViewComboDelegate(QObject *parent = 0);

    QString wichIngredient;

    ReteteViewComboDelegateIngredient(QList<Ingredient*> itemList, QObject *parent = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    QList<Ingredient*> items;

signals:

public slots:

};

#endif // RETETEVIEWCOMBODELEGATEINGREDIENT_H
