#ifndef WARNINGYOUUSEDALLINGREDIENTS_H
#define WARNINGYOUUSEDALLINGREDIENTS_H

#include <QDialog>

namespace Ui {
class WarningYouUsedAllIngredients;
}

class WarningYouUsedAllIngredients : public QDialog
{
    Q_OBJECT

public:
    explicit WarningYouUsedAllIngredients(QWidget *parent = 0);
    ~WarningYouUsedAllIngredients();

private slots:
    void on_WarningYouUsedAllIngredients_finished(int result);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::WarningYouUsedAllIngredients *ui;
};

#endif // WARNINGYOUUSEDALLINGREDIENTS_H
