#ifndef RENAMRETETA_H
#define RENAMRETETA_H

#include <QDialog>

namespace Ui {
class RenamReteta;
}

class RenamReteta : public QDialog
{
    Q_OBJECT

public:
    explicit RenamReteta(QWidget *parent = 0);
    ~RenamReteta();

private slots:
    void on_pushButton_clicked();

    void on_RenamReteta_finished(int result);

private:
    Ui::RenamReteta *ui;
};

#endif // RENAMRETETA_H
