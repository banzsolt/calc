#include "WarningForDeleteRetete.h"
#include "ui_WarningForDeleteRetete.h"

#include "MainWindow.h"
#include "ReteteView.h"
#include "ui_ReteteView.h"

WarningForDeleteRetete::WarningForDeleteRetete(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WarningForDeleteRetete)
{
    ui->setupUi(this);
    ui->label->setText("Sigur doriti sa stergeti retea " + MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->name);
}

WarningForDeleteRetete::~WarningForDeleteRetete()
{
    delete ui;
}

void WarningForDeleteRetete::on_Nu_clicked()
{
    this->close();
    this->deleteLater();
}

void WarningForDeleteRetete::on_WarningForDeleteRetete_finished(int result)
{
    if (!result)
    {
        this->close();
        this->deleteLater();
    }
}

void WarningForDeleteRetete::on_Da_clicked()
{
    ReteteView::m_Instance->ui->listaRetete->clear();

    MainWindow::m_Instance->listaRetete->removeAt(ReteteView::m_Instance->currentSelectedRow);
    MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

    this->close();
    this->deleteLater();

    for (int i = 0; i < MainWindow::m_Instance->listaRetete->length(); ++i)
    {
        ReteteView::m_Instance->ui->listaRetete->addItem(MainWindow::m_Instance->listaRetete->at(i)->name);
    }
}
