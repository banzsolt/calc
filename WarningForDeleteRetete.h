#ifndef WARNINGFORDELETERETETE_H
#define WARNINGFORDELETERETETE_H

#include <QDialog>

namespace Ui {
class WarningForDeleteRetete;
}

class WarningForDeleteRetete : public QDialog
{
    Q_OBJECT

public:
    explicit WarningForDeleteRetete(QWidget *parent = 0);
    ~WarningForDeleteRetete();

private slots:
    void on_Nu_clicked();

    void on_WarningForDeleteRetete_finished(int result);

    void on_Da_clicked();

private:
    Ui::WarningForDeleteRetete *ui;
};

#endif // WARNINGFORDELETERETETE_H
