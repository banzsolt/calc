#ifndef SQDMULTICOLUMNLISTWIDGET_H
#define SQDMULTICOLUMNLISTWIDGET_H

#include <QListView>

class SQDMultiColumnListWidget : public QListView
{
    Q_OBJECT
public:
    explicit SQDMultiColumnListWidget(QWidget *parent = 0);

signals:

public slots:

};

#endif // SQDMULTICOLUMNLISTWIDGET_H
