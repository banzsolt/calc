#include "RenamReteta.h"
#include "ui_RenamReteta.h"
#include <QDebug>

#include "MainWindow.h"
#include "ReteteView.h"
#include "ui_ReteteView.h"

RenamReteta::RenamReteta(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RenamReteta)
{
    ui->setupUi(this);
}

RenamReteta::~RenamReteta()
{
    delete ui;
}

void RenamReteta::on_pushButton_clicked()
{
    MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->name = this->ui->NumeRetetaNoua->text();
    MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

    ReteteView::m_Instance->ui->listaRetete->clear();

    this->close();
    this->deleteLater();

    for (int i = 0; i < MainWindow::m_Instance->listaRetete->length(); ++i)
    {
        ReteteView::m_Instance->ui->listaRetete->addItem(MainWindow::m_Instance->listaRetete->at(i)->name);
    }
}

void RenamReteta::on_RenamReteta_finished(int result)
{
    if (!result)
    {



        this->close();
        this->deleteLater();
    }
}
