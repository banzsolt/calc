#include "VanzariView.h"
#include "ui_VanzariView.h"
#include "MainWindow.h"

VanzariView* VanzariView::m_Instance = 0;

VanzariView::VanzariView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VanzariView)
{
    m_Instance = this;
    chestiiVandute = new QList<int>;

    ui->setupUi(this);
    items = *MainWindow::m_Instance->listaRetete;

    ui->vanzariTreeView->setFocusPolicy(Qt::NoFocus);

    model = new VanzariViewListModel(items);
    ui->vanzariTreeView->setModel(model);
}

VanzariView::~VanzariView()
{
    delete ui;
}

void VanzariView::on_vanzariTreeView_clicked(const QModelIndex &index)
{
}
