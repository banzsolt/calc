#include "VanzariViewListModel.h"
#include "Backend/ChestiiVandute.h"
#include "VanzariView.h"


VanzariViewListModel::VanzariViewListModel(QObject *parent) :
    QAbstractListModel(parent)
{

}

VanzariViewListModel::VanzariViewListModel(QList<Reteta*> ingrediente, QObject* parent)
    : QAbstractListModel(parent)
{

    reteteList = ingrediente;

    for (int i = 0; i < reteteList.count(); ++i)
    {
        VanzariView::m_Instance->chestiiVandute->append(0);
    }
}

VanzariViewListModel::~VanzariViewListModel()
{
}

int VanzariViewListModel::rowCount(const QModelIndex& ) const
{
    return reteteList.size();
}

int VanzariViewListModel::columnCount(const QModelIndex &parent) const
{
    return 7;
}

void VanzariViewListModel::setComboData(int row, QString name)
{
    reteteList.value(row)->name = name;
}

QVariant VanzariViewListModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        if (index.column() == 0)
            return reteteList.value(index.row())->name;

        if (index.column() == 1)
        {
            return VanzariView::m_Instance->chestiiVandute->value(index.row());
        }

        if (index.column() == 2)
        {
            return reteteList.value(index.row())->pret;
        }

        if (index.column() == 3)
        {
            return reteteList.value(index.row())->pretDeVanzare;
        }

        if (index.column() == 4)
        {
            return VanzariView::m_Instance->chestiiVandute->value(index.row()) * reteteList.value(index.row())->pret;
        }

        if (index.column() == 5)
        {
            return VanzariView::m_Instance->chestiiVandute->value(index.row()) * reteteList.value(index.row())->pretDeVanzare;
        }

        if (index.column() == 6)
        {
            return VanzariView::m_Instance->chestiiVandute->value(index.row()) * reteteList.value(index.row())->pretDeVanzare - VanzariView::m_Instance->chestiiVandute->value(index.row()) * reteteList.value(index.row())->pret;
        }
    }

    if(role == Qt::UserRole)
    {

    }

    if(role == Qt::EditRole)
    {

    }

    if(role == Qt::ToolTipRole)
    {
        return "Hex code: " + reteteList.value(index.row())->name;
    }

    return QVariant();
}



QVariant VanzariViewListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
        switch(section)
        {
        case 0:
            return ("Nume materie prima");
        case 1:
            return ("Vandute");
        case 2:
            return ("Cost productie");
        case 3:
            return ("Vanzarea produsului");
        case 4:
            return ("Cost total");
        case 5:
            return ("Incasare totala");
        case 6:
            return ("Profit");


        default:
            return QVariant();
        }
    }
    else
    {
        return QString("Color %1").arg(section);
    }
}

bool VanzariViewListModel::insertRows(int position, int rows, const QModelIndex &index)
{
}

bool VanzariViewListModel::removeRows(int position, int rows, const QModelIndex &index)
{
}

bool VanzariViewListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{


    if(index.isValid() && role == Qt::EditRole)
    {
        int row = index.row();

        if ((index.column() == 0) || (index.column() == 2))
            return false;
        else
        {
            VanzariView::m_Instance->chestiiVandute->takeAt(row);
            VanzariView::m_Instance->chestiiVandute->insert(row, int(value.value<int>()));
            emit(dataChanged(index, index));

            return true;
        }
    }



    if(index.isValid() && role == Qt::UserRole)
    {

        return true;
    }


    return false;
}

Qt::ItemFlags VanzariViewListModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}
