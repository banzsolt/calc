#ifndef RETETEVIEWCOMBODELEGATE_H
#define RETETEVIEWCOMBODELEGATE_H

#include <QItemDelegate>
#include "Backend/ingredient.h"

class ReteteViewComboDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    //explicit ReteteViewComboDelegate(QObject *parent = 0);

    ReteteViewComboDelegate(QList<Ingredient*> itemList, QObject *parent = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    QList<Ingredient*> items;

signals:

public slots:

};

#endif // RETETEVIEWCOMBODELEGATE_H
