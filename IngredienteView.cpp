#include "IngredienteView.h"
#include "ui_IngredienteView.h"
#include <QDebug>
#include "MainWindow.h"
#include "Backend/Ingredient.h"
#include "QMessageBox"

IngredienteView::IngredienteView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IngredienteView)
{
    ui->setupUi(this);
    ui->listaIngredients->clear();
    currentSelectedIngredient = 0;
    for (int i = 0; i < MainWindow::m_Instance->listaIngred->count(); ++i)
        ui->listaIngredients->addItem(MainWindow::m_Instance->listaIngred->value(i)->name);
}

IngredienteView::~IngredienteView()
{
    delete ui;
}

void IngredienteView::on_adaugareIngredient_clicked()
{
    Ingredient *iNou = new Ingredient("Ingredient", 0, 10, -1);
    MainWindow::m_Instance->listaIngred->push_back(iNou);
    MainWindow::m_Instance->readFromFile->WriteListIngredienteChange(MainWindow::m_Instance->listaIngred);

    ui->listaIngredients->addItem("Ingredient");
}


void IngredienteView::on_numeIngredientTextbox_textChanged(const QString &arg1)
{
    if (ui->numeIngredientTextbox == QApplication::focusWidget())
    {
        int i = 0;
        for(i = 0 ;i < MainWindow::m_Instance->listaIngred->size(); ++i)
        {
            if (MainWindow::m_Instance->listaIngred->value(i)->GetName() == ui->listaIngredients->currentItem()->text())
            {
                break;
            }
        }
        Ingredient *newIngredient = MainWindow::m_Instance->listaIngred->value(ui->listaIngredients->currentRow());
        MainWindow::m_Instance->listaIngred->removeAt(i);
        newIngredient->SetName(arg1);
        MainWindow::m_Instance->listaIngred->insert(i, newIngredient);
        MainWindow::m_Instance->readFromFile->WriteListIngredienteChange(MainWindow::m_Instance->listaIngred);
        MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

        ui->listaIngredients->currentItem()->setText(arg1);
    }
}

void IngredienteView::on_listaIngredients_itemClicked(QListWidgetItem *item)
{
    if (item)
    {
        ui->numeIngredientTextbox->setText(item->text());
        nameOfEditedItem = item->text();

        for (int i = 0; i < MainWindow::m_Instance->listaIngred->count(); ++i)
        {
            if(MainWindow::m_Instance->listaIngred->value(i)->GetName() == item->text())
            {
                ui->pretTextbox->setText(QString::number(MainWindow::m_Instance->listaIngred->value(i)->GetPret()));
                ui->tipProdusCombobox->clear();
                ui->tipProdusCombobox->addItem(MainWindow::m_Instance->listaIngred->value(i)->GetType());
                ui->unitateTextbox->setText(QString::number(MainWindow::m_Instance->listaIngred->value(i)->GetUnitateDeMasura()));
            }
        }
    }
}

void IngredienteView::on_pretTextbox_textChanged(const QString &arg1)
{
    if (ui->pretTextbox == QApplication::focusWidget())
    {
        int i = 0;
        for(i = 0 ;i < MainWindow::m_Instance->listaIngred->size(); ++i)
        {
            if (MainWindow::m_Instance->listaIngred->value(i)->GetName() == ui->listaIngredients->currentItem()->text())
            {
                break;
            }
        }
        Ingredient *newIngredient = MainWindow::m_Instance->listaIngred->value(ui->listaIngredients->currentRow());
        MainWindow::m_Instance->listaIngred->removeAt(i);
        newIngredient->SetPret(arg1.toDouble());
        MainWindow::m_Instance->listaIngred->insert(i,newIngredient);
        MainWindow::m_Instance->readFromFile->WriteListIngredienteChange(MainWindow::m_Instance->listaIngred);
        MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);
    }
}

void IngredienteView::on_unitateTextbox_textChanged(const QString &arg1)
{
    if (ui->unitateTextbox == QApplication::focusWidget())
    {
        int i = 0;
        for(i = 0 ;i < MainWindow::m_Instance->listaIngred->size(); ++i)
        {
            if (MainWindow::m_Instance->listaIngred->value(i)->GetName() == ui->listaIngredients->currentItem()->text())
            {
                break;
            }
        }
        Ingredient *newIngredient = MainWindow::m_Instance->listaIngred->value(ui->listaIngredients->currentRow());
        MainWindow::m_Instance->listaIngred->removeAt(i);
        newIngredient->SetUnitateDeMasura(arg1.toInt());
        MainWindow::m_Instance->listaIngred->insert(i,newIngredient);
        MainWindow::m_Instance->readFromFile->WriteListIngredienteChange(MainWindow::m_Instance->listaIngred);
        MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);
    }
}

void IngredienteView::on_removeIngredient_clicked()
{
    if (ui->listaIngredients->count() > 0)
    {
        int reply;
        reply = QMessageBox::warning(this, "Sterge semipreparatul",
                             "Doriti sa stergeti semipreparatul: " + nameOfEditedItem,
                             "Da" ,"Ups","         Nu         ",2, -1); //QMessageBox::No);
        if (reply == 0)
        {
            ui->listaIngredients->clear();
            for (int j = 0; j < MainWindow::m_Instance->listaRetete->count(); ++j)
            {
                QMap<Ingredient*, int>::iterator it = MainWindow::m_Instance->listaRetete->at(j)->ingrediente.begin();
                while(!(it == MainWindow::m_Instance->listaRetete->at(j)->ingrediente.end()))
                {
                    if (it.key() == MainWindow::m_Instance->listaIngred->at(currentSelectedIngredient))
                    {
                        MainWindow::m_Instance->listaRetete->at(j)->RemoveIngredient(it.key());
                    }
                    it++;
                }
            }
            MainWindow::m_Instance->listaIngred->removeAt(currentSelectedIngredient);

            for (int i = 0; i < MainWindow::m_Instance->listaIngred->count(); ++i)
                ui->listaIngredients->addItem(MainWindow::m_Instance->listaIngred->value(i)->name);

            MainWindow::m_Instance->readFromFile->WriteListIngredienteChange(MainWindow::m_Instance->listaIngred);
            MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);
        }

    }
}

void IngredienteView::on_listaIngredients_clicked(const QModelIndex &index)
{
    currentSelectedIngredient = index.row();
}
