#ifndef RAPORTVIEW_H
#define RAPORTVIEW_H

#include <QWidget>
#include "Backend/ingredient.h"
#include <QDate>

namespace Ui {
class RaportView;
}

class RaportView : public QWidget
{
    Q_OBJECT

public:
    QString date;
    QDate theDate;
    explicit RaportView(QWidget *parent = 0);
    QMap<Ingredient*, int> total;
    ~RaportView();

private slots:
    void on_pushButton_clicked();

    void on_calendarWidget_clicked(const QDate &date1);

    void on_lineEdit_textChanged(const QString &arg1);


private:
    Ui::RaportView *ui;
};

#endif // RAPORTVIEW_H
