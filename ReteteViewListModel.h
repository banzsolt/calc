#ifndef RETETEVIEWLISTMODEL_H
#define RETETEVIEWLISTMODEL_H

#include <QAbstractListModel>
#include "Backend/Ingredient.h"

class ReteteViewListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    ReteteViewListModel(QObject *parent = 0);
    ReteteViewListModel(QList<Ingredient*> ingrediente, QObject *parent = 0);
    ~ReteteViewListModel();

    enum Roles {ComboRole = Qt::UserRole};

    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int selection, Qt::Orientation orientation, int role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent) const;

    void setComboData(int row, QString name);

    bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex());
    bool insertIngredient(Ingredient *newIngredient, int position, int rows, const QModelIndex &parent = QModelIndex());
    bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex());
    bool setData(const QModelIndex &index, const QVariant &value, int role);


    Qt::ItemFlags flags(const QModelIndex &index) const;

    QList<Ingredient*> ingredienteList;

};

#endif // RETETEVIEWLISTMODEL_H
