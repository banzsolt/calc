#include "ingredient.h"

#include <QString>
#include <QDebug>
#include <QtNetwork/QtNetwork>

Ingredient::Ingredient()
{
    this->name = "nothing";
    this->id = 0;
    this->unitateDeMasura = 0;
    this->pret = 0;
    this->type = "-";
}

Ingredient::Ingredient(QString name, int unitateDeMasura, double pret, int id, QString type)
{
    this->id = id;
    this->name = name;
    this->unitateDeMasura = unitateDeMasura;
    this->pret = pret;
    this->type = type;
}




void Ingredient::SetName(QString newName)
{
    this->name = newName;
}

void Ingredient::SetPret(double newPret)
{
    this->pret = newPret;
}

void Ingredient::SetUnitateDeMasura(int newUnitateDeMasura)
{
    this->unitateDeMasura = newUnitateDeMasura;
}

void Ingredient::SetID(int newID)
{
    this->id = newID;
}

void Ingredient::SetType(QString newType)
{
    this->type = newType;
}

QString Ingredient::GetName()
{
    return this->name;
}

double Ingredient::GetPret()
{
    return this->pret;
}

QString Ingredient::GetType()
{
    return this->type;
}

int Ingredient::GetUnitateDeMasura()
{
    return this->unitateDeMasura;
}

bool Ingredient::operator ==(Ingredient &arg)
{
    return this->name == arg.name;
}

QString Ingredient::getMacAddress()
{
//    foreach(QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
//    {
//          // Return only the first non-loopback MAC Address
//        if (!(netInterface.flags() & QNetworkInterface::IsLoopBack))
//        return netInterface.hardwareAddress();
//    }
    return QString();
}

