#ifndef LISTINGREDIENT_H
#define LISTINGREDIENT_H

#include <QList>
#include "ingredient.h"
#include <iostream>
#include <QString>

using namespace std;

class ListIngredient
{
private:
    QList<Ingredient> list;
public:
    //Constructor
    ListIngredient();

    //Add - adds ingredient to the list
    void Add(Ingredient newIngredient);

    //Remove - removes the ingredient from the list
    void Remove(QString theIngredient);

    //ChangeName
    void ChangeName(QString theIngredient, QString newName);

    //ChangePret
    void ChangePret(QString theIngredient, double newPret);

    //ChangeUnitateDeMasura
    void ChangeUnitateDeMasura(QString theIngredient, int newUnitateDeMasura);

    //ReturnName - returns list.value(i)
    QString ReturnName(int i);

    //ReturnPret
    double ReturnPret(int i);

    //ReturnList - returns the list
    QList<Ingredient> ReturnList();

    //ReturnUnitateDeMasura
    int ReturnUnitateDeMasura(int i);

    //ReturnLength
    int ReturnLength();

    //ReturnType
    QString ReturnType(int i);

    void clearTheShit();


};

#endif // LISTINGREDIENT_H
