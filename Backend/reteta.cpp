#include "reteta.h"

Reteta::Reteta()
{

}

Reteta::Reteta(QString name, bool semipreparat, int unitateDeMasura, float pret)
{
    this->name = name;
    this->isSemipreparat = semipreparat;
    this->unitateDeMasura = unitateDeMasura;
    this->pret = 0;
}

void Reteta::Update()
{
    this->pret = 0;
    QMap <Ingredient* , int>::iterator it = ingrediente.begin();
    for (int i = 0 ; i < ingrediente.count(); ++i)
    {
        this->pret += it.key()->pret / it.key()->unitateDeMasura * it.value();
        it++;
    }

    QMap <Reteta*, int>::iterator ite = semipreparate.begin();
    for (int i = 0 ; i < semipreparate.count(); ++i)
    {
        ite.key()->Update();
        this->pret += ite.key()->pret / ite.key()->unitateDeMasura * ite.value();
        ite++;
    }
}


void Reteta::AddIngredient(Ingredient *ingredient, int cantitate)
{
    this->ingrediente[ingredient] = cantitate;
    pret += ingredient->pret;
}

void Reteta::AddSemipreparat(Reteta *semipreparat, int cantitate)
{
    this->semipreparate[semipreparat] = cantitate;
    pret += semipreparat->pret;
}

void Reteta::RemoveIngredient(Ingredient *ingredient)
{
    this->ingrediente.remove(ingredient);
}

void Reteta::RemoveSemipreparat(Reteta *semipreparat)
{
    this->semipreparate.remove(semipreparat);
}

QString Reteta::ReturnIngredient(int alCatelea)
{
    QMap<Ingredient*, int>::iterator it = this->ingrediente.begin();
    for (int i = 0 ; i < alCatelea ; ++i)
    {
        it++;
    }
    return it.key()->name;
}

QString Reteta::ReturnSemipreparat(int alCatelea)
{
    QMap<Reteta*, int>::iterator it = this->semipreparate.begin();
    for (int i = 0 ; i < alCatelea ; ++i)
    {
        it++;
    }
    return it.key()->name;
}

void Reteta::ChangeIngredient(QString wichIngredient, Ingredient *toWhat, QList<Ingredient*>* list)
{
    QMap <Ingredient*, int>::iterator it = ingrediente.begin();

    for (int i = 0; i < this->ingrediente.count() ; ++i)
    {
        if (it.key()->name == wichIngredient)
        {
            for (int j = 0; j < list->size(); ++j)
            {
                if (list->at(j) == toWhat)
                {
                    int cantitate = it.value();
                    this->RemoveIngredient(it.key());
                    this->AddIngredient(list->at(j), cantitate);
                }
            }
            break;
        }
        it++;
    }
}

void Reteta::ChangeSemipreparat(QString wichIngredient, Reteta *toWhat, QList<Reteta *> *list)
{
    QMap <Reteta*, int>::iterator it = semipreparate.begin();

    for (int i = 0; i < this->semipreparate.count() ; ++i)
    {
        if (it.key()->name == wichIngredient)
        {
            for (int j = 0; j < list->size(); ++j)
            {
                if (list->at(j) == toWhat)
                {
                    int cantitate = it.value();
                    this->RemoveSemipreparat(it.key());
                    this->AddSemipreparat(list->at(j), cantitate);
                }
            }
            break;
        }
        it++;
    }
}

void Reteta::SetNameReteta(QString newName)
{
    this->name = newName;
}

void Reteta::SetPretReteta(float newPret)
{
    int pretTotal = 0;

    foreach (Ingredient *ingredient, this->ingrediente.keys())
    {
        pretTotal += ingredient->GetPret() * this->ingrediente.value(ingredient)/ingredient->GetUnitateDeMasura();
    }

    this->pret = newPret;
}

void Reteta::SetUnitateDeMasuraReteta(int newUnitateDeMasura)
{
    this->unitateDeMasura = newUnitateDeMasura;
}

void Reteta::SetSemipreparatReteta(bool isSemipreparat)
{
    this->isSemipreparat = isSemipreparat;
}

int Reteta::GetCantitatePtIngredient(Ingredient *ingredient)
{
    foreach (Ingredient *iteratedIngredient, this->ingrediente.keys())
    {
        if (iteratedIngredient == ingredient)
        {
            return this->ingrediente.value(ingredient, 0);
        }
    }
}

int Reteta::GetCantitatePtIngredient(int IngredientNumber)
{
    int i = 0;
    foreach (Ingredient *iteratedIngredient, this->ingrediente.keys())
    {
        if (i == IngredientNumber)
        {
            return this->ingrediente.value(iteratedIngredient, 0);
        }
        i++;
    }
}

void Reteta::SetCantitatePtIngredient(Ingredient *ingredient, int newCantitate)
{
    this->ingrediente[ingredient] = newCantitate;
}

void Reteta::SetCantitatePtIngredient(int IngredientNumber, int newCantitate)
{
    int i = 0;
    foreach (Ingredient *iteratedIngredient, this->ingrediente.keys())
    {
        if (i == IngredientNumber)
        {
            this->ingrediente[iteratedIngredient] = newCantitate;
        }
        i++;
    }
}

int Reteta::GetCantitatePtSemipreparat(Reteta *reteta)
{
    foreach (Reteta *iteratedReteta, this->semipreparate.keys())
    {
        if (iteratedReteta == reteta)
        {
            return this->semipreparate.value(reteta, 0);
        }
    }
}

int Reteta::GetCantitatePtSemipreparat(int RetetaNumber)
{
    int i = 0;
    foreach (Reteta *iteratedReteta, this->semipreparate.keys())
    {
        if (i == RetetaNumber)
        {
            return this->semipreparate.value(iteratedReteta, 0);
        }
        i++;
    }
}

void Reteta::SetCantitatePtSemipreparat(Reteta *reteta, int newCantitate)
{
    this->semipreparate[reteta] = newCantitate;
}

void Reteta::SetCantitatePtSemipreparat(int RetetaNumber, int newCantitate)
{
    int i = 0;
    foreach (Reteta *iteratedReteta, this->semipreparate.keys())
    {
        if (i == RetetaNumber)
        {
            this->semipreparate[iteratedReteta] = newCantitate;
        }
        i++;
    }
}






