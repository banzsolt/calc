#-------------------------------------------------
#
# Project created by QtCreator 2014-01-18T17:17:15
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Pizza
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ingredient.cpp \
    reteta.cpp \
    listingredient.cpp \
    ListReteta.cpp \
    ReadFromFile.cpp

HEADERS  += mainwindow.h \
    ingredient.h \
    reteta.h \
    listingredient.h \
    ListReteta.h \
    ReadFromFile.h

FORMS    += mainwindow.ui
