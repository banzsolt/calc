#ifndef READFROMFILE_H
#define READFROMFILE_H

#include <QFile>
#include "listingredient.h"
#include "ListReteta.h"
#include "ingredient.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

class ReadFromFile
{
private:
    int idLastIngredient;
    int idLastReteta;
public:
    QList<Ingredient*>              list;
    QList<Reteta*>                  listReteta;
    QJsonObject                     ingredient;
    QJsonArray                      ingrediente;
    QJsonObject                     allingrediente;
    QJsonObject                     reteta;
    QJsonArray                      retete;
    QJsonArray                      retetaIngrediente;
    QJsonObject                     retetaIngredient;
    QJsonArray                      retetaSemipreparate;
    QJsonObject                     retetaSemipreparat;

    ReadFromFile();

    //ReadListIngrediente - returns the list of the ingredients from the file
    QList<Ingredient *> ReadListIngrediente();

    //WriteListIngrediente - writes all the ignredients in the file
    void WriteListIngrediente(Ingredient theIngredient);

    //WriteListIngrediente - writes all the ignredients in the file
    void WriteListIngredienteChange(ListIngredient list);

    //WriteListIngrediente - writes all the ignredients in the file
    void WriteListIngredienteChange(QList<Ingredient *> lista);

    //WriteListIngrediente - writes all the ignredients in the file
    void WriteListIngredienteChange(QList<Ingredient *> *lista);

    //ReadListRetete - returns the list of retete from the file
    QList<Reteta*> ReadListRetete(QList<Ingredient*> listaCuIngrediente);

    //WriteListRetete - writes all the retetes in the file
    void WriteListRetete(Reteta theReteta);

    //WriteListReteteChange - writes all the ingredients in the file
    void WriteListReteteChange(ListReteta list);

    //WriteListReteteChange - writes all the ingredients in the file
    void WriteListReteteChange(QList<Reteta *> list);

    //WriteListReteteChange - writes all the ingredients in the file
    void WriteListReteteChange(QList<Reteta*> *list);

//    //WritesTheRaport
//    void WriteRaport(QMap<Ingredient*, int> list);

//    //ReadTheRaport
//    QMap<Ingredient*, int> ReadRaport(QList<Ingredient*> listaCuIngrediente);

};

#endif // READFROMFILE_H
