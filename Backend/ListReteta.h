#ifndef LISTRETETA_H
#define LISTRETETA_H

#include <QMap>
#include "reteta.h"
#include <QString>


class ListReteta
{
private:
        QList<Reteta> listretete;
public:
    //Constructor
    ListReteta();

    //Add - adds ingredient to the list
    void Add(Reteta newReteta);

    //Add - adds ingredient to the list
    void AddIngredient(Reteta *theReteta, Ingredient *ingredient, int cantitate);

    //RemoveIngredient - removes ingredient from the reteta
    void RemoveIngredient(Reteta* theReteta, Ingredient* ingredient);

    //Remove - removes the ingredient from the list
    void Remove(QString theReteta);

    //ChangeTheCantitate - changes the cantitate of an Ingredient
    void ChangeTheCantitate(QString theReteta, Ingredient* theIngredient, int newCantitate);

    //ChangeName
    void ChangeName(QString theReteta, QString newName);

    //ChangeID
    void ChangeID(QString theReteta, int newID);

    //ChangeUnitateDeMasura
    void ChangeUnitateDeMasura(QString theReteta, int newUnitateDeMasura);

    //ReturnList - returns the list
    QList<Reteta> ReturnList();

    //ReturnRetea
    Reteta ReturnReteta(int i);

    //ReturnName - returns list.value(i)
    QString ReturnName(int i);

    //ReturnUnitateDeMasura
    int ReturnUnitateDeMasura(int i);

    //ReturnSize
    int ReturnSize();

    //ReturnType
    bool ReturnType(int i);

    //ReturnID
    int ReturnID(int i);

    //ReturnIngrediente
    QMap<Ingredient *, int> ReturnIngreiente(int i);

//    //ReturnIteratorBegining
//    QMap<QString, int>::Iterator ReturnIteratorBegin(int i);

//    //ReturnIteratorEnd
//    QMap<QString, int>::Iterator ReturnIteratorEnd(int i);

    //ReturnLength
    int ReturnLength();
};

#endif // LISTRETETA_H
