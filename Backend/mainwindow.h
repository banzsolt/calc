#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "listingredient.h"
#include "ListReteta.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include "ReadFromFile.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    QJsonObject result;
    QJsonObject ingredient;
    QJsonArray ingrediente;
    QJsonObject reteta;
    QJsonArray retete;
    QJsonArray retetaIngrediente;
    QJsonObject retetaIngredient;
    QJsonObject allingrediente;



    ReadFromFile wqert;
    int idLastReteta;
    int idLastIngredient;

    ListReteta listreteta;


    ListIngredient list;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

public:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
