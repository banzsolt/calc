/*
Copyright
All right reserved by Zsolt Ban on 21.01.2014
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "reteta.h"
#include "ingredient.h"
#include "listingredient.h"
#include <QDebug>
#include <QFile>
#include <QString>
#include <QCryptographicHash>
#include <QtGui>
#include "ListReteta.h"
#include "ReadFromFile.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    Reteta proba("proba", 1);
    proba.SetName("proba");
    proba.SetPret(1000);
    proba.SetUnitateDeMasura(1);
    //listreteta.Add(proba);
    //listreteta.ChangeName(proba, "nouuuuuuuuuuuuuuu");
    //listreteta.Remove("nouuuuuuuuuuuuujuu");
    //Reteta *proba_2 = new Reteta("nimic", 0);

    //qDebug() << listreteta.ReturnName(0);
    //qDebug() << listreteta.ReturnSize();



    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("//out.txt");
    QFile file(*fileLocation);
    delete fileLocation;
    if (file.exists())
    {
        file.open(QIODevice::ReadOnly | QIODevice::Text);

        while (!file.atEnd())
        {
            QByteArray line = file.readLine();

        }
    }
    else
    {
        Ingredient *macAddress = new Ingredient();
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        QString encoder;
        encoder = macAddress->getMacAddress();

        //aici vine codarea




        //delete encoder;
        delete macAddress;
    }


    file.close();


    list = wqert.ReadListIngrediente();
    //qDebug() << list.ReturnLength();





    idLastReteta = -1;
    ui->setupUi(this);

    ui->lineEdit_2->setPlaceholderText("Unitate de masura");
    ui->lineEdit_3->setPlaceholderText("Pret");
}

MainWindow::~MainWindow()
{
    delete ui;
}

//new ingredient for the list
void MainWindow::on_pushButton_clicked()
{
    Ingredient *newIngredient = new Ingredient(ui->lineEdit->text().toLower(),
                                               ui->lineEdit_2->text().toInt(),
                                               ui->lineEdit_3->text().toDouble(),
                                               ++idLastIngredient);
    newIngredient->SetType(ui->lineEdit_8->text().toLower());
    ui->comboBox->addItem(newIngredient->GetName());
    ui->comboBox_2->addItem(newIngredient->GetName());

    list.Add(*newIngredient);

    wqert.WriteListIngrediente(*newIngredient);

}

//new reteta
void MainWindow::on_pushButton_3_clicked()
{
    Reteta *newReteta = new Reteta(ui->lineEdit_4->text().toLower(), ++idLastReteta);
    //reteta["name"] = newReteta->GetName();
    //reteta["unitatedemasura"] = 1;
    ui->comboBox_3->addItem(newReteta->GetName());
    //retete.append(reteta);
    listreteta.Add(*newReteta);

    //while ()

    /*QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/retete.json");
    QFile file(*fileLocation);
    QTextStream out(&file);
    delete fileLocation;
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument d;
    d.setArray(retete);
    QByteArray data ;
    data = d.toJson();
    out << data;
    file.close();*/

}

//change ingredient
void MainWindow::on_pushButton_2_clicked()
{
    list.ChangeUnitateDeMasura(list.ReturnName(ui->comboBox->currentIndex()),
                                ui->lineEdit_5->text().toInt());
    list.ChangePret(list.ReturnName(ui->comboBox->currentIndex()),
                     ui->lineEdit_6->text().toDouble());
    wqert.WriteListIngredienteChange(list);
}

//change reteta
void MainWindow::on_pushButton_4_clicked()
{

    Reteta proba_2;
    proba_2.SetName("marcaramelizat");
    proba_2.SetUnitateDeMasura(1);
    QString *proba = new QString("iiii");
    proba_2.Add(proba, 60);
    listreteta.Add(proba_2);
    //listreteta.ChangeName("marcaramelizat", "proba");
    //QString *proba = new QString();
    //*proba = "iiii";
    //listreteta.AddIngredient("proba",proba, 60);

    qDebug() << listreteta.ReturnName(0);

    while (!retete.empty())
    {
        retete.removeFirst();
    }
    int i;
    for ( i = 0; i < listreteta.ReturnLength(); ++i)
    {
        while (!retetaIngrediente.empty())
        {
            retetaIngrediente.removeFirst();
        }

        QMap<QString, int> ingredientele;
        ingredientele = proba_2.GetIngredients();
        QMap<QString, int>::Iterator it = ingredientele.begin();
        while (!(it == ingredientele.end()))
        {
            retetaIngredient["name"] = it.key();
            retetaIngredient["cantitate"] = it.value();
            retetaIngrediente.append(retetaIngredient);
            it++;
        }
        reteta["ingrediente"] = retetaIngrediente;
        reteta["name"] = listreteta.ReturnName(i);
        retete.append(reteta);
        qDebug() << ingredientele.size();
    }


    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/retete.json");
    QFile file_2(*fileLocation);
    QTextStream out(&file_2);
    delete fileLocation;
    file_2.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument e;
    e.setArray(retete);
    QByteArray data ;
    data = e.toJson();
    out << data;
    file_2.close();


/*
    retetaIngredient["name"] = ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    retetaIngredient["cantitate"] = ui->lineEdit_7->text().toInt();

    retetaIngrediente.append(retetaIngredient);
    reteta["ingrediente"] = retetaIngrediente;
    retete.insert(ui->comboBox_3->currentIndex(), reteta);
    retete.removeAt(ui->comboBox_3->currentIndex()+1);


    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/retete.json");
    QFile file_2(*fileLocation);
    QTextStream out(&file_2);
    delete fileLocation;
    file_2.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument e;
    e.setArray(retete);
    QByteArray data ;
    data = e.toJson();
    out << data;
    file_2.close();
*/
}

