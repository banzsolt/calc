#include "ListReteta.h"

ListReteta::ListReteta()
{

}

void ListReteta::Add(Reteta newReteta)
{
    listretete.push_front(newReteta);
}

void ListReteta::AddIngredient(Reteta* theReteta, Ingredient* ingredient, int cantitate)
{
    bool ok = false;
    int i;
    for (i = 0; i < listretete.length() ; ++i)
    {
        if (listretete.value(i).name == theReteta->name)
        {
            ok = true;
            break;
        }
    }

    if (ok)
    {
        Reteta *aux = new Reteta();
        *aux = listretete.value(i);
        aux->AddIngredient(ingredient, cantitate);
        listretete.replace(i, *aux);
        //listretete.removeAt(i);
        //listretete.insert(i, *aux);
        delete aux;
    }
}

void ListReteta::RemoveIngredient(Reteta* theReteta, Ingredient* ingredient)
{
    bool ok = false;
    int i;
    for (i = 0; i < listretete.length() ; ++i)
    {
        if (listretete.value(i).name == theReteta->name)
        {
            ok = true;
            break;
        }
    }

    if (ok)
    {
        Reteta *aux = new Reteta();
        *aux = listretete.value(i);
        aux->ingrediente.remove(ingredient);
        listretete.removeAt(i);
        listretete.insert(i, *aux);
        delete aux;
    }
}

void ListReteta::Remove(QString theReteta)
{
    bool ok = false;
    int i;
    for (i = 0; i < listretete.length(); ++i)
    {
        if(listretete.value(i).name == theReteta)
        {
            ok = true;
            break;
        }
    }
    if (ok)
    {
        listretete.removeAt(i);
    }
}

void ListReteta::ChangeTheCantitate(QString theReteta, Ingredient* theIngredient, int newCantitate)
{
    bool ok = false;
    int i;
    for (i = 0; i < listretete.length(); ++i)
    {
        if(listretete.value(i).name == theReteta)
        {
            ok = true;
            break;
        }
    }

    if (ok)
    {
        Reteta *aux = new Reteta();
        *aux = listretete.value(i);
        aux->ingrediente.value(theIngredient, newCantitate);
        listretete.removeAt(i);
        listretete.insert(i, *aux);
        delete aux;
    }
}

void ListReteta::ChangeName(QString theReteta, QString newName)
{
    bool ok = false;
    int i;
    for (i = 0; i < listretete.length(); ++i)
    {
        if(listretete.value(i).name == theReteta)
        {
            ok = true;
            break;
        }
    }
    if (ok)
    {
        Reteta aux; //= new Reteta(theReteta, 0);
        aux = listretete.value(i);
        aux.name = newName;
        listretete.replace(i, aux);
        //listretete.removeAt(i);
        //listretete.insert(i,*aux);
        //delete aux;
    }
}

void ListReteta::ChangeID(QString theReteta, int newID)
{
    bool ok = false;
    int i;
    for (i = 0; i < listretete.length(); ++i)
    {
        if(listretete.value(i).name == theReteta)
        {
            ok = true;
            break;
        }
    }
    if (ok)
    {
        Reteta *aux = new Reteta(theReteta, 0);
        *aux = listretete.value(i);
        listretete.removeAt(i);
        listretete.insert(i,*aux);
        delete aux;
    }
}

void ListReteta::ChangeUnitateDeMasura(QString theReteta, int newUnitateDeMasura)
{
    bool ok = false;
    int i;
    for (i = 0; i < listretete.length(); ++i)
    {
        if(listretete.value(i).name == theReteta)
        {
            ok = true;
            break;
        }
    }
    if (ok)
    {
        Reteta *aux = new Reteta(theReteta, 0);
        *aux = listretete.value(i);
        aux->unitateDeMasura = newUnitateDeMasura;
        listretete.removeAt(i);
        listretete.insert(i,*aux);
        delete aux;
    }
}

QList<Reteta> ListReteta::ReturnList()
{
    return listretete;
}

Reteta ListReteta::ReturnReteta(int i)
{
    return listretete.value(i);
}

QString ListReteta::ReturnName(int i)
{
    return listretete.value(i).name;
}

int ListReteta::ReturnUnitateDeMasura(int i)
{
    return listretete.value(i).unitateDeMasura;
}

int ListReteta::ReturnSize()
{
    return listretete.size();
}

bool ListReteta::ReturnType(int i)
{
    return listretete.value(i).isSemipreparat;
}

int ListReteta::ReturnID(int i)
{
    return 0;//listretete.value(i).GetID();
}

QMap<Ingredient*, int> ListReteta::ReturnIngreiente(int i)
{
    return listretete.value(i).ingrediente;
}

//QMap<QString, int>::Iterator ListReteta::ReturnIteratorBegin(int i)
//{
//    return listretete.value(i).GetIteratorBegin();
//}

//QMap<QString, int>::Iterator ListReteta::ReturnIteratorEnd(int i)
//{
//    return listretete.value(i).GetIteratorEnd();
//}

int ListReteta::ReturnLength()
{
    return listretete.length();
}


