#ifndef RETETA_H
#define RETETA_H

#include "Ingredient.h"
#include <QMap>
#include <QString>


class Reteta
{

public:
    QMap<Ingredient*, int>      ingrediente;
    QMap<Reteta*, int>          semipreparate;

    QString                     name;
    bool                        isSemipreparat;
    int                         unitateDeMasura;
    double                      pret;
    double                      pretDeVanzare;

    Reteta();
    Reteta(QString name,
           bool semipreparat = false,
           int unitateDeMasura = 0,
           float pret = 0.0f);

    void            Update();

    void            AddIngredient(Ingredient* ingredient, int cantitate);
    void            AddSemipreparat(Reteta* semipreparat, int cantitate);

    void            RemoveIngredient(Ingredient* ingredient);
    void            RemoveSemipreparat(Reteta* semipreparat);
    QString         ReturnIngredient(int alCatelea);
    QString         ReturnSemipreparat(int alCatelea);

    void            ChangeIngredient(QString wichIngredient, Ingredient *toWhat, QList<Ingredient*>* list);
    void            ChangeSemipreparat(QString wichIngredient, Reteta *toWhat, QList<Reteta*>* list);

    void            SetNameReteta(QString newName);
    void            SetPretReteta(float newPret);
    void            SetUnitateDeMasuraReteta(int newUnitateDeMasura);
    void            SetSemipreparatReteta(bool isSemipreparat);

    int             GetCantitatePtIngredient(Ingredient* ingredient);
    int             GetCantitatePtIngredient(int IngredientNumber);
    void            SetCantitatePtIngredient(Ingredient* ingredient, int newCantitate);
    void            SetCantitatePtIngredient(int IngredientNumber, int newCantitate);

    int             GetCantitatePtSemipreparat(Reteta* reteta);
    int             GetCantitatePtSemipreparat(int RetetaNumber);
    void            SetCantitatePtSemipreparat(Reteta* reteta, int newCantitate);
    void            SetCantitatePtSemipreparat(int RetetaNumber, int newCantitate);
};

#endif // RETETA_H
