#ifndef CHESTIIVANDUTE_H
#define CHESTIIVANDUTE_H
#include "Backend/Reteta.h"


class ChestiiVandute
{

public:
            QMap<Reteta*, int> lista;


            ChestiiVandute();

void        SetVanzare(Reteta* reteta, int cat);
void        Pair(QList<Reteta *> list);
int         GetCantitate(int alCatelea);
void        SetCantitate(int randul, int cantiatea);

};

#endif // CHESTIIVANDUTE_H
