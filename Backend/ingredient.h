#ifndef INGREDIENT_H
#define INGREDIENT_H

#include <QString>
#include <QtNetwork/QNetworkInterface>

class Ingredient
{
public:
    int id;
    QString name;
    int unitateDeMasura;
    double pret;
    QString type;

    Ingredient();
    Ingredient(QString name,
               int unitateDeMasura,
               double pret,
               int id,
               QString type = "-");

    void            SetName(QString newName);
    void            SetPret(double newPret);
    void            SetUnitateDeMasura(int newUnitateDeMasura);
    void            SetID(int newID);
    void            SetType(QString newType);

    QString         GetName();
    double          GetPret();
    QString         GetType();
    int             GetUnitateDeMasura();
    bool            operator ==(Ingredient &arg);

    QString         getMacAddress();
};

#endif // INGREDIENT_H
