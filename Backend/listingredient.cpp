#include "listingredient.h"
#include <iostream>
#include <QString>
#include <QDebug>

using namespace std;

ListIngredient::ListIngredient()
{
}

void ListIngredient::Add(Ingredient newIngredient)
{
    list.push_back(newIngredient);
}

void ListIngredient::Remove(QString theIngredient)
{
    bool ok = false;
    int i;
    for (i = 0; i < list.length(); ++i)
    {
        if(list.value(i).GetName() == theIngredient)
        {
            ok = true;
            break;
        }
    }
    if (ok)
        list.removeAt(i);
}

void ListIngredient::ChangeName(QString theIngredient, QString newName)
{
    bool ok = false;
    int i;
    for (i = 0; i < list.length(); ++i)
    {
        if(list.value(i).GetName() == theIngredient)
        {
            ok = true;
            break;
        }
    }
    if (ok)
    {
        //list.value(i).SetName(newName);
        Ingredient *aux = new Ingredient();
        *aux = list.value(i);
        aux->SetName(newName);
        list.removeAt(i);
        list.insert(i,*aux);
        delete aux;
    }
}

void ListIngredient::ChangePret(QString theIngredient, double newPret)
{
    bool ok = false;
    int i;
    for (i = 0; i < list.length(); ++i)
    {
        if(list.value(i).GetName() == theIngredient)
        {
            ok = true;
            break;
        }
    }
    if (ok)
    {
        //list.value(i).SetPret(newPret);
        Ingredient *aux = new Ingredient();
        *aux = list.value(i);
        aux->SetPret(newPret);
        list.removeAt(i);
        list.insert(i,*aux);
        delete aux;
    }
}

void ListIngredient::ChangeUnitateDeMasura(QString theIngredient, int newUnitateDeMasura)
{
    bool ok = false;
    int i;
    for (i = 0; i < list.length(); ++i)
    {
        if(list.value(i).GetName() == theIngredient)
        {
            ok = true;
            break;
        }
    }
    if (ok)
    {
        //list.value(i).SetUnitateDeMasura(newUnitateDeMasura);
        Ingredient *aux = new Ingredient();
        *aux = list.value(i);
        aux->SetUnitateDeMasura(newUnitateDeMasura);
        list.removeAt(i);
        list.insert(i,*aux);
        delete aux;
    }
}

QString ListIngredient::ReturnName(int i)
{
    return list.value(i).GetName();
}

double ListIngredient::ReturnPret(int i)
{
    return list.value(i).GetPret();
}

QList<Ingredient> ListIngredient::ReturnList()
{
    return list;
}

int ListIngredient::ReturnUnitateDeMasura(int i)
{
    return list.value(i).GetUnitateDeMasura();
}

int ListIngredient::ReturnLength()
{
    return list.length();
}

QString ListIngredient::ReturnType(int i)
{
    return list.value(i).GetType();
}

void ListIngredient::clearTheShit()
{
    list.clear();
}


