#include "ReadFromFile.h"
#include <QtGui>

ReadFromFile::ReadFromFile()
{
    //qDebug() << listReteta.ReturnLength();
}

QList<Ingredient*> ReadFromFile::ReadListIngrediente()
{
    list.clear();
    QString *fileLocationa = new QString;
    *fileLocationa = QCoreApplication::applicationDirPath();
    fileLocationa->append("/ingrediente.json");
    QFile file(*fileLocationa);
    delete fileLocationa;
    QString all;
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    all = file.readAll();
    file.close();

    QJsonDocument sd = QJsonDocument::fromJson(all.toUtf8());

    QJsonObject jsonObject = sd.object();

    QJsonArray jsonArray = jsonObject["Ingrediente"].toArray();

    idLastIngredient = -1;
    foreach (const QJsonValue & value, jsonArray)
    {
        QJsonObject obj = value.toObject();
        Ingredient *newIngredient = new Ingredient();
        newIngredient->name = obj.value("name").toString();
        newIngredient->unitateDeMasura = obj.value("unitateDeMasura").toDouble();
        newIngredient->pret = obj.value("pret").toDouble();
        newIngredient->type = obj.value("type").toString();
        newIngredient->id = ++idLastIngredient;
        list.push_back(newIngredient);
    };
    return list;
}

void ReadFromFile::WriteListIngrediente(Ingredient theIngredient)
{
//    list.append(theIngredient);
    int i;

    while (!ingrediente.empty())
    {
        ingrediente.removeFirst();
    }

//    for (i = 0; i<list.ReturnLength(); ++i)
//    {
//        ingredient["name"] = list.ReturnName(i);
//        ingredient["unitateDeMasura"] = list.ReturnUnitateDeMasura(i);
//        ingredient["pret"] =  list.ReturnPret(i);
//        ingredient["type"] = list.ReturnType(i);
//        ingrediente.append(ingredient);
//    }


    allingrediente["Ingrediente"] = ingrediente;

    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/ingrediente.json");
    QFile file(*fileLocation);
    delete fileLocation;


    QTextStream out(&file);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument d;
    d.setObject(allingrediente);
    QByteArray data ;
    data = d.toJson();
    out << data;
    file.close();
}

void ReadFromFile::WriteListIngredienteChange(ListIngredient list)
{
    list.clearTheShit();
    int i;
    while (!ingrediente.empty())
    {
        ingrediente.removeFirst();
    }

    for (i = 0; i<list.ReturnLength(); ++i)
    {
        ingredient["name"] = list.ReturnName(i);
        ingredient["unitateDeMasura"] = list.ReturnUnitateDeMasura(i);
        ingredient["pret"] =  list.ReturnPret(i);
        ingredient["type"] = list.ReturnType(i);
        ingrediente.append(ingredient);
    }


    allingrediente["Ingrediente"] = ingrediente;

    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/ingrediente.json");
    QFile file(*fileLocation);
    delete fileLocation;


    QTextStream out(&file);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument d;
    d.setObject(allingrediente);
    QByteArray data ;
    data = d.toJson();
    out << data;
    file.close();
}

void ReadFromFile::WriteListIngredienteChange(QList<Ingredient*> lista)
{
    list.clear();
    int i;
    while (!ingrediente.empty())
    {
        ingrediente.removeFirst();
    }

    for (i = 0; i<lista.size(); ++i)
    {
        ingredient["name"] = lista.value(i)->GetName();
        ingredient["unitateDeMasura"] = lista.value(i)->GetUnitateDeMasura();
        ingredient["pret"] =  lista.value(i)->GetPret();
        ingredient["type"] = lista.value(i)->GetType();
        ingrediente.append(ingredient);
    }


    allingrediente["Ingrediente"] = ingrediente;

    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/ingrediente.json");
    QFile file(*fileLocation);
    delete fileLocation;


    QTextStream out(&file);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument d;
    d.setObject(allingrediente);
    QByteArray data ;
    data = d.toJson();
    out << data;
    file.close();
}

void ReadFromFile::WriteListIngredienteChange(QList<Ingredient*> *lista)
{
    list.clear();
    int i;
    while (!ingrediente.empty())
    {
        ingrediente.removeFirst();
    }


    for (i = 0; i < lista->size(); ++i)
    {
        ingredient["name"] = lista->value(i)->GetName();
        ingredient["unitateDeMasura"] = lista->value(i)->GetUnitateDeMasura();
        ingredient["pret"] =  lista->value(i)->GetPret();
        ingredient["type"] = lista->value(i)->GetType();
        ingrediente.append(ingredient);
    }


    allingrediente["Ingrediente"] = ingrediente;


    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/ingrediente.json");
    QFile file(*fileLocation);
    delete fileLocation;

    QTextStream out(&file);
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    QJsonDocument d;
    d.setObject(allingrediente);
    QByteArray data ;
    data = d.toJson();
    out << data;
    file.close();
}

QList<Reteta*> ReadFromFile::ReadListRetete(QList<Ingredient*> listaCuIngrediente)
{


    QString *fileLocationa = new QString;
    *fileLocationa = QCoreApplication::applicationDirPath();
    fileLocationa->append("/retete.json");
    QFile file(*fileLocationa);
    delete fileLocationa;
    QString all;
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    all = file.readAll();
    file.close();
    QJsonDocument sd = QJsonDocument::fromJson(all.toUtf8());

    QJsonArray jsonArray = sd.array();
    //qDebug() << jsonArray;
    foreach (const QJsonValue & value, jsonArray)
    {
        QJsonObject obj = value.toObject();
        Reteta *newReteta = new Reteta;
        newReteta->name = obj.value("name").toString();
        newReteta->pret = obj.value("pret").toDouble();
        newReteta->pretDeVanzare = obj.value("pretDeVanzare").toDouble();
        newReteta->unitateDeMasura = obj.value("unitateDeMasura").toInt();



        QJsonArray ingredienteDeRetete;
        ingredienteDeRetete = obj.value("ingrediente").toArray();
        foreach (const QJsonValue & value_2, ingredienteDeRetete)
        {
            QJsonObject obj_2 = value_2.toObject();
            QString numeIngredient = obj_2.value("name").toString();
            foreach (Ingredient* ingredient, listaCuIngrediente)
            {
                if (ingredient->name == numeIngredient)
                {
                    newReteta->AddIngredient(ingredient, obj_2.value("cantitate").toInt());
                }
            }
        }



        QJsonArray semipreparateDeReteta;
        semipreparateDeReteta = obj.value("semipreparate").toArray();
        foreach (const QJsonValue & value_2, semipreparateDeReteta)
        {
            QJsonObject obj_2 = value_2.toObject();
            QString numeIngredient = obj_2.value("name").toString();
            foreach (Reteta* ingredient, listReteta)
            {
                if (ingredient->name == numeIngredient)
                {
                    newReteta->AddSemipreparat(ingredient, obj_2.value("cantitate").toInt());
                }
            }
        }

        listReteta.push_back(newReteta);

    }
    return listReteta;
}

void ReadFromFile::WriteListRetete(Reteta theReteta)
{
//    listReteta.Add(theReteta);
//    while (!retete.empty())
//    {
//        retete.removeFirst();
//    }
//    int i;
//    for ( i = 0; i < listReteta.ReturnLength(); ++i)
//    {
//        while (!retetaIngrediente.empty())
//        {
//            retetaIngrediente.removeFirst();
//        }

//        QMap<Ingredient*, int> ingredientele;
//        ingredientele = listReteta.ReturnIngreiente(i);
//        QMap<Ingredient*, int>::Iterator it = ingredientele.begin();
//        while (!(it == ingredientele.end()))
//        {
//            retetaIngredient["name"] = it.key();
//            retetaIngredient["cantitate"] = it.value();
//            retetaIngrediente.append(retetaIngredient);
//            it++;
//        }
//        reteta["ingrediente"] = retetaIngrediente;
//        reteta["name"] = listReteta.ReturnName(i);
//        retete.append(reteta);
//        //qDebug() << ingredientele.size();
//    }


    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/retete.json");
    QFile file_2(*fileLocation);
    QTextStream out(&file_2);
    delete fileLocation;
    file_2.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument e;
    e.setArray(retete);
    QByteArray data ;
    data = e.toJson();
    out << data;
    file_2.close();
}

void ReadFromFile::WriteListReteteChange(ListReteta list)
{
//    while (!retete.empty())
//    {
//        retete.removeFirst();
//    }
//    int i;
//    for ( i = 0; i < list.ReturnLength(); ++i)
//    {
//        while (!retetaIngrediente.empty())
//        {
//            retetaIngrediente.removeFirst();
//        }

//        QMap<QString, int> ingredientele;
//        ingredientele = list.ReturnIngreiente(i);
//        QMap<QString, int>::Iterator it = ingredientele.begin();
//        while (!(it == ingredientele.end()))
//        {
//            retetaIngredient["name"] = it.key();
//            retetaIngredient["cantitate"] = it.value();
//            retetaIngrediente.append(retetaIngredient);
//            it++;
//        }
//        reteta["ingrediente"] = retetaIngrediente;
//        reteta["name"] = list.ReturnName(i);
//        retete.append(reteta);
//        //qDebug() << ingredientele.size();
//    }


//    QString *fileLocation = new QString;
//    *fileLocation = QCoreApplication::applicationDirPath();
//    fileLocation->append("/retete.json");
//    QFile file_2(*fileLocation);
//    QTextStream out(&file_2);
//    delete fileLocation;
//    file_2.open(QIODevice::WriteOnly | QIODevice::Text);
//    QJsonDocument e;
//    e.setArray(retete);
//    QByteArray data ;
//    data = e.toJson();
//    out << data;
//    file_2.close();
}

void ReadFromFile::WriteListReteteChange(QList<Reteta*> list)
{
    while (!retete.empty())
    {
        retete.removeFirst();
    }
    int i;
    for ( i = 0; i < list.size(); ++i)
    {
        while (!retetaIngrediente.empty())
        {
            retetaIngrediente.removeFirst();
        }
        while(!retetaSemipreparate.empty())
        {
            retetaSemipreparate.removeFirst();
        }

        QMap<Ingredient*, int> ingredientele;
        ingredientele = list.value(i)->ingrediente;
        QMap<Ingredient*, int>::Iterator it = list.value(i)->ingrediente.begin();
        while (!(it == ingredientele.end()))
        {
            Ingredient *newIngredient = it.key();
            retetaIngredient["name"] = newIngredient->name;
            retetaIngredient["cantitate"] = it.value();
            retetaIngrediente.append(retetaIngredient);
            it++;
        }
        QMap<Reteta*, int>::iterator it_2 = list.value(i)->semipreparate.begin();
        while (!(it_2 == list.value(i)->semipreparate.end()))
        {
            Reteta *newReteta = it_2.key();
            retetaSemipreparat["name"] = newReteta->name;
            retetaSemipreparat["cantitate"] = it_2.value();
            retetaSemipreparate.append(retetaSemipreparat);
            it_2++;
        }


        reteta["ingrediente"] = retetaIngrediente;
        reteta["name"] = list.value(i)->name;
        retete.append(reteta);
        //qDebug() << ingredientele.size();
    }


    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/retete.json");
    QFile file_2(*fileLocation);
    QTextStream out(&file_2);
    delete fileLocation;
    file_2.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument e;
    e.setArray(retete);
    QByteArray data ;
    data = e.toJson();
    out << data;
    file_2.close();
}

void ReadFromFile::WriteListReteteChange(QList<Reteta *> *list)
{
    while (!retete.empty())
    {
        retete.removeFirst();
    }


    int i;
    for ( i = 0; i < list->size(); ++i)
    {
        while (!retetaIngrediente.empty())
        {
            retetaIngrediente.removeFirst();
        }
        while (!retetaSemipreparate.empty())
        {
            retetaSemipreparate.removeFirst();
        }

        QMap<Ingredient*, int>::Iterator it = list->value(i)->ingrediente.begin();
        while (!(it == list->value(i)->ingrediente.end()))
        {
            Ingredient *newIngredient = it.key();
            retetaIngredient["name"] = newIngredient->name;
            retetaIngredient["cantitate"] = it.value();
            retetaIngrediente.append(retetaIngredient);
            it++;
        }
        QMap<Reteta*, int>::iterator it_2 = list->value(i)->semipreparate.begin();
        while (!(it_2 == list->value(i)->semipreparate.end()))
        {
            Reteta *newReteta = it_2.key();
            retetaSemipreparat["name"] = newReteta->name;
            retetaSemipreparat["cantitate"] = it_2.value();
            retetaSemipreparate.append(retetaSemipreparat);
            it_2++;
        }
        reteta["ingrediente"] = retetaIngrediente;
        reteta["semipreparate"] = retetaSemipreparate;
        reteta["name"] = list->value(i)->name;
        reteta["pret"] = list->value(i)->pret;
        reteta["pretDeVanzare"] = list->value(i)->pretDeVanzare;
        reteta["unitateDeMasura"] = list->value(i)->unitateDeMasura;
        retete.append(reteta);
        //qDebug() << ingredientele.size();
    }


    QString *fileLocation = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    fileLocation->append("/retete.json");
    QFile file_2(*fileLocation);
    QTextStream out(&file_2);
    delete fileLocation;
    file_2.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument e;
    e.setArray(retete);
    QByteArray data ;
    data = e.toJson();
    out << data;
    file_2.close();
}

////void ReadFromFile::WriteRaport(QMap<Ingredient *, int> list, QString unde)
//{
//    while (!list.empty())
//    {

//    }
//}

//QMap<Ingredient *, int> ReadFromFile::ReadRaport(QList<Ingredient *> listaCuIngrediente)
//{

//}
