#include "ReteteViewComboDelegateSemipreparat.h"
#include "MainWindow.h"
#include <QComboBox>
#include "ReteteView.h"
#include "ui_ReteteView.h"
#include "Backend/reteta.h"



ReteteViewComboDelegateSemipreparat::ReteteViewComboDelegateSemipreparat(QList<Reteta *> itemList, QObject *parent)
{
        items = itemList;
}

QWidget *ReteteViewComboDelegateSemipreparat::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QComboBox *editor = new QComboBox(parent);
    QStringList list ;

    //qDebug() << "index.row= "<<index.row();


    for (int i = 0; i < MainWindow::m_Instance->listaRetete->count(); ++i)
    {

        Reteta newReteta;
        newReteta = *MainWindow::m_Instance->listaRetete->at(i);
        int count = 0;
        QMap<Reteta*, int>::iterator it = MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->semipreparate.begin();
        for (int j = 0; j < MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->semipreparate.count(); ++j)
        {
            qDebug() << it.key()->name << "==" << newReteta.name;
            qDebug() << newReteta.name << "=" <<  MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->name;
            if(it.key()->name == newReteta.name)
            {
                qDebug() << "break";
                break;
            }
            else
            {
                if (!(newReteta.name == MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->name ))
                {
                    count++;
                }
            }
            qDebug() << count;

            it++;
        }
        if (count == MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->semipreparate.count())
        {
            editor->addItem(newReteta.name);
        }
    }

    return editor;
}

void ReteteViewComboDelegateSemipreparat::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString value = index.model()->data(index, Qt::DisplayRole).toString();
    QComboBox *comboBox = static_cast<QComboBox*>(editor);

    for (int i = 0; i<MainWindow::m_Instance->listaRetete->count() ; ++i)
    {
        if (MainWindow::m_Instance->listaRetete->at(i)->name == comboBox->currentText())
        {
            Reteta *theReteta = new Reteta();
            theReteta = MainWindow::m_Instance->listaRetete->at(i);

            QString inWhat = MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->ReturnSemipreparat(index.row());

            MainWindow::m_Instance->listaRetete->value(ReteteView::m_Instance->currentSelectedRow)->ChangeSemipreparat(inWhat, theReteta, MainWindow::m_Instance->listaRetete);
            MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

            //Aici resetat sa se vada datele noi!
        }
    }
}

void ReteteViewComboDelegateSemipreparat::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    QString value = comboBox->currentText();
    model->setData(index, value, Qt::UserRole);
}

void ReteteViewComboDelegateSemipreparat::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}
