#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "ui_ReteteView.h"
#include "ReteteView.h"
#include "ui_VanzariView.h"

MainWindow* MainWindow::m_Instance = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    readFromFile = new ReadFromFile();

    listaIngred = new QList<Ingredient*>;
    listaRetete = new QList<Reteta*>;

    *listaIngred = readFromFile->ReadListIngrediente();
    qDebug() << "ajunge  pana aici";
    *listaRetete = readFromFile->ReadListRetete(*listaIngred);


    ui->setupUi(this);

    m_Instance = this;

//    Reteta proba;
//    proba.SetName("aaaa");
//    proba.Add("iiii", 50);
//    QMap<QString, int>::iterator it = proba.GetIngredients().begin();
    //qDebug() << it.key();










    raportView = new RaportView;
    reteteView = new ReteteView;
    vanzariView = new VanzariView;
    ingredienteView = new IngredienteView;


    this->ui->centralLayout->addWidget(reteteView);
    reteteView->ui->listaRetete->setCurrentRow(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionRetete_triggered()
{
    this->ui->centralLayout->takeAt(0)->widget()->hide();
    this->ui->centralLayout->addWidget(reteteView);
    reteteView->show();

    ReteteView::m_Instance->ui->listaRetete->clear();

    for (int i = 0; i < MainWindow::m_Instance->listaRetete->length(); ++i)
    {
        ReteteView::m_Instance->ui->listaRetete->addItem(MainWindow::m_Instance->listaRetete->at(i)->name);
    }

     ReteteView::m_Instance->ui->listaRetete->setCurrentRow(0);
}

void MainWindow::on_actionIngrediente_triggered()
{
    this->ui->centralLayout->takeAt(0)->widget()->hide();
    this->ui->centralLayout->addWidget(ingredienteView);
    ingredienteView->show();
}

void MainWindow::on_actionVanzari_triggered()
{
    this->ui->centralLayout->takeAt(0)->widget()->hide();
    this->ui->centralLayout->addWidget(vanzariView);

    vanzariView->model = new VanzariViewListModel(*listaRetete);
    vanzariView->ui->vanzariTreeView->setModel(vanzariView->model);
    vanzariView->show();
}

void MainWindow::on_actionRaport_triggered()
{
    this->ui->centralLayout->takeAt(0)->widget()->hide();
    this->ui->centralLayout->addWidget(raportView);
    raportView->show();
}
