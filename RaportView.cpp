#include "RaportView.h"
#include "ui_RaportView.h"
#include <QFile.h>
#include "qdebug.h"
#include "MainWindow.h"
#include "QDate"
#include "QTextDocumentWriter"
#include "VanzariViewListModel.h"
#include <QDir>
#include <QUrl>
#include <QDesktopServices>

RaportView::RaportView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RaportView)
{

    ui->setupUi(this);

    for (int k = 0; k < MainWindow::m_Instance->listaIngred->count(); ++k)
    {
        total[MainWindow::m_Instance->listaIngred->at(k)] = 0;
    }
}

RaportView::~RaportView()
{
    delete ui;
}

void RaportView::on_pushButton_clicked()
{
    total.empty();
    QString printable;
    QDate *null = new QDate;
    if (theDate == *null)
    {
        theDate = QDate::currentDate();
        date.append(printable.setNum(theDate.day()));
        date.append(".");
        date.append(printable.setNum(theDate.month()));
        date.append(".");
        date.append(printable.setNum(theDate.year()));
    }


    if (!(ui->lineEdit->text().toInt() == 0))
    {

        QMap<Ingredient*, int> change;
        QMap<Ingredient*, int>::iterator it = total.begin();

        for (int i = 0; i < total.count(); ++i)
        {
            if (it.value() > 0)
            {
                change[it.key()] = it.value();
            }
        it++;
        }


        QMap<Ingredient*, int>::iterator it_2 = change.begin();
        for (int i = 0; i < change.count(); ++i)
        {
                        qDebug() <<  "pretul vechi" <<         it_2.key()->pret;
                        qDebug() << "cate sunt" << change.count();
                        qDebug() << "cantitate" << it_2.value();
            it_2.key()->pret += ui->lineEdit->text().toDouble() /change.count()/it_2.value();
            qDebug() <<     "pretul nou" <<       it_2.key()->pret;
            it_2++;
        }

        for (int i = 0; i < MainWindow::m_Instance->listaRetete->count(); ++i)
        {
            MainWindow::m_Instance->listaRetete->at(i)->Update();
        }

        QMap <Ingredient* , int>::iterator iter = total.begin();
        for (int i = 0; i < total.count(); ++i)
        {
            iter++;
        }

    }




    QMap<Reteta*, int> vanzari;
    for (int i = 0; i < MainWindow::m_Instance->listaRetete->count(); ++i)
    {
        vanzari[MainWindow::m_Instance->listaRetete->at(i)] = VanzariView::m_Instance->chestiiVandute->at(i);
    }

    for (int k = 0; k < MainWindow::m_Instance->listaIngred->count(); ++k)
    {
        total[MainWindow::m_Instance->listaIngred->at(k)] = 0;
    }


    QMap<Reteta*, int>::iterator it = vanzari.begin();
    for (int j =0; j < vanzari.count(); ++j)
    {

        QMap<Ingredient*, int>::iterator it_1 = it.key()->ingrediente.begin();
        for (int k = 0; k < it.key()->ingrediente.count(); ++k)
        {

            QMap<Ingredient*, int>::iterator it_2 = total.begin();
            while (!(it_1.key()->name == it_2.key()->name))
            {
                it_2++;
            }
            total[it_2.key()] += it.value() * it_1.value();
            it_1++;
        }

        if(!(it.key()->semipreparate.count() == 0))
        {
        QMap<Reteta*, int>::iterator it_3 = it.key()->semipreparate.begin();
        for (int i = 0; i < it_3.key()->semipreparate.count(); ++i)
        {
            QMap<Ingredient*, int>::iterator it_4 = it_3.key()->ingrediente.begin();
            for (int k = 0; k < it_3.key()->ingrediente.count(); ++k)
            {

                QMap<Ingredient*, int>::iterator it_5 = total.begin();
                while (!(it_4.key()->name == it_5.key()->name))
                {
                    it_5++;
                }
                total[it_5.key()] += it_3.value() * it_4.value();
                it_4++;
            }
            it_3++;
        }
        }
        it++;
    }







    QString all;                                            //all = string with the raport of ingredients
    QString all_2;                                          //all = string with the raport of vanzari
    all = "<html><head><style type='text/css'>";
    all = all + ".tg{border-collapse:collapse;border-spacing:0;border-color:#999;}";
    all = all + ".tg td{font-family:Arial, sans-serif;font-size:14px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;width:300px;text-align: center;}.tg .nume{text-align: left;}.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}</style></head><body><table class=" + "'tg' " + "style=" + "'table-layout: fixed;width: 100%;'" + "><colgroup><col style=" + " 'width: 7s7px' " + " ><col style=" + " 'width: 77px' " + "><col style=" + " 'width: 76px' " + "></colgroup><tr><th class=" + " 'tg-031e' " + ">Nume Ingredient</th><th class=" + " 'tg-031e' " + ">Vanzari</th><th class=" + " 'tg-031e' " + ">Cost</th></tr>";
    all_2 = "<html><head><style type='text/css'>";
    all_2 = all_2 + ".tg{border-collapse:collapse;border-spacing:0;border-color:#999;}";
    all_2 = all_2 + ".tg td{font-family:Arial, sans-serif;font-size:14px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;width:300px;text-align: center;}.tg .nume{text-align: left;}.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}</style></head><body><table class=" + "'tg' " + "style=" + "'table-layout: fixed;width: 100%;'" + "><colgroup><col style=" + " 'width: 7s7px' " + " ><col style=" + " 'width: 77px' " + "><col style=" + " 'width: 76px' " + "></colgroup><tr><th class=" + " 'tg-031e' " + ">Nume Produs</th><th class=" + " 'tg-031e' " + ">Cantitatea vandute</th><th class=" + " 'tg-031e' " + ">Cost productie</th><th class=" + " 'tg-031e' " + ">Vanzarea produsului</th><th class=" + " 'tg-031e' " + ">Cost total</th><th class=" + " 'tg-031e' " + ">Incasare totala</th><th class=" + " 'tg-031e' " + ">Profit</th></tr>";

    for (int i = 0; i < MainWindow::m_Instance->listaRetete->count(); ++i)
    {
        if(!(VanzariView::m_Instance->chestiiVandute->at(i) == 0))
        {
        all_2 = all_2 + '\n' + "    <tr>";
        all_2 = all_2 + '\n' + "        <td class='"+"tg-031e nume"+"'>" + MainWindow::m_Instance->listaRetete->at(i)->name + "</td>";
        all_2 = all_2 + '\n' + "        <td class='"+"tg-031e"+"'>" + printable.number(VanzariView::m_Instance->chestiiVandute->at(i)) + "</td>";
        all_2 = all_2 + '\n' + "         <td class='"+"tg-031e"+"'>" + printable.number(MainWindow::m_Instance->listaRetete->at(i)->pret) +"</td>";
        all_2 = all_2 + '\n' + "        <td class='"+"tg-031e nume"+"'>" + printable.number(MainWindow::m_Instance->listaRetete->at(i)->pretDeVanzare) + "</td>";
        all_2 = all_2 + '\n' + "        <td class='"+"tg-031e"+"'>" + printable.number(VanzariView::m_Instance->chestiiVandute->at(i) * MainWindow::m_Instance->listaRetete->at(i)->pret) + "</td>";
        all_2 = all_2 + '\n' + "         <td class='"+"tg-031e"+"'>" + printable.number(VanzariView::m_Instance->chestiiVandute->at(i) * MainWindow::m_Instance->listaRetete->at(i)->pretDeVanzare) +"</td>";
        all_2 = all_2 + '\n' + "        <td class='"+"tg-031e"+"'>" + printable.number((VanzariView::m_Instance->chestiiVandute->at(i) * MainWindow::m_Instance->listaRetete->at(i)->pretDeVanzare - VanzariView::m_Instance->chestiiVandute->at(i) * MainWindow::m_Instance->listaRetete->at(i)->pret)) + "</td>";
        all_2 = all_2 + '\n' + "      </tr>";
        }
    }

    QMap<Ingredient*, int>::iterator tot = total.begin();
    int grandTotal = 0;
    for (int i=0 ; i < MainWindow::m_Instance->listaIngred->count(); ++i)
    {
        if (!(tot.value() == 0))
        {
        all = all + '\n' + "    <tr>";
        all = all + '\n' + "        <td class='"+"tg-031e nume"+"'>" + MainWindow::m_Instance->listaIngred->at(i)->name + "</td>";
        all = all + '\n' + "        <td class='"+"tg-031e"+"'>" + printable.number(tot.value()) + "</td>";
        all = all + '\n' + "         <td class='"+"tg-031e"+"'>" + printable.number(tot.value() * tot.key()->pret / tot.key()->unitateDeMasura)  +"</td>";//* MainWindow::m_Instance->listaIngred->at(i)->pret / MainWindow::m_Instance->listaIngred->at(i)->unitateDeMasura) +"</td>";
        all = all + '\n' + "      </tr>";
        grandTotal += tot.value() * tot.key()->pret / tot.key()->unitateDeMasura;
        tot++;
        }
    }


    //all_2 = all_2 + "   <tr><td class= " + "'tg-031e'" + "colspan="+"'5'"+">" "</td><td class="+"'tg-031e'"+">";
    //all_2 = all_2 + "</td></tr></table></body></html>";
    all = all + "   <tr><td class= " + "'tg-031e'" + "colspan="+"'2'"+">" "</td><td class="+"'tg-031e'"+">"+printable.number(grandTotal);
    all = all + "</td></tr></table></body></html>";




    QString *fileLocation = new QString;
    QString *fileLocation_2 = new QString;
    *fileLocation = QCoreApplication::applicationDirPath();
    *fileLocation_2 = QCoreApplication::applicationDirPath();

    QDir a;
    a.setPath(*fileLocation);
    QString month;
    switch (theDate.month())
    {
    case 1:
        month = "Ianuarie";
        break;
    case 2:
        month = "Februarie";
        break;
    case 3:
        month = "Martie";
        break;
    case 4:
        month = "Aprilie";
        break;
    case 5:
        month = "Mai";
        break;
    case 6:
        month = "Junie";
        break;
    case 7:
        month = "Julie";
        break;
    case 8:
        month = "August";
        break;
    case 9:
        month = "Septembrie";
        break;
    case 10:
        month = "Octombire";
        break;
    case 11:
        month = "Noiembrie";
        break;
    case 12:
        month = "Decembrie";
        break;
    default:
        break;
    }


    if (!a.mkdir("Raport"))
    {
        a.setPath(*fileLocation + "/Raport/");
        if(!a.mkdir(month))
        {
            fileLocation->append("/Raport/" + month + "/Raport - " + date + ".html");
            fileLocation_2->append("/Raport/" + month + "/Vanzari - " + date + ".html");
        }
        else
        {
            //facut raportul de sfasit de luna
            fileLocation->append("/Raport/" + month + "/Raport - " + date + ".html");
            fileLocation_2->append("/Raport/" + month + "/Vanzari - " + date + ".html");
        }
    }
    else
    {
        //facut raportul de sfarsit de luna
        a.setPath(*fileLocation + "/Raport/");
        a.mkdir(month);
        fileLocation->append("/Raport/" + month + "/Raport - " + date + ".html");
        fileLocation_2->append("/Raport/" + month + "/Vanzari - " + date + ".html");
    }



    QFile file_1(*fileLocation);
    QFile file_2(*fileLocation_2);
    QTextStream out(&file_1);
    file_1.open(QIODevice::WriteOnly | QIODevice::Text);

    out << all;

    QDesktopServices::openUrl(QUrl::fromLocalFile(*fileLocation));
    file_1.close();

    QTextStream out_2(&file_2);
    file_2.open(QIODevice::WriteOnly | QIODevice::Text);

    out_2 << all_2;

    QDesktopServices::openUrl(QUrl::fromLocalFile(*fileLocation_2));
    file_2.close();

}


void RaportView::on_calendarWidget_clicked(const QDate &date1)
{

    date = "";
    QString printable;
    date.append(printable.setNum(date1.day()));
    date.append(".");
    date.append(printable.setNum(date1.month()));
    date.append(".");
    date.append(printable.setNum(date1.year()));
    theDate = date1;
}

void RaportView::on_lineEdit_textChanged(const QString &arg1)
{
//    if (!(arg1.toInt() == 0))
//    {

//        QMap<Ingredient*, int> change;
//        QMap<Ingredient*, int>::iterator it = total.begin();

//        for (int i = 0; i < total.count(); ++i)
//        {
//            if (it.value() > 0)
//            {
//                change[it.key()] = it.value();
//            }
//        it++;
//        }


//        QMap<Ingredient*, int>::iterator it_2 = change.begin();
//        for (int i = 0; i < change.count(); ++i)
//        {
//                        qDebug() <<  "pretul vechi" <<         it_2.key()->pret;
//                        qDebug() << "cate sunt" << change.count();
//                        qDebug() << "cantitate" << it_2.value();
//            it_2.key()->pret += arg1.toDouble()/change.count()/it_2.value();
//            qDebug() <<     "pretul nou" <<       it_2.key()->pret;
//            it_2++;
//        }

//        for (int i = 0; i < MainWindow::m_Instance->listaRetete->count(); ++i)
//        {
//            MainWindow::m_Instance->listaRetete->at(i)->Update();
//        }

//        QMap <Ingredient* , int>::iterator iter = total.begin();
//        for (int i = 0; i < total.count(); ++i)
//        {
//            iter++;
//        }

//    }
}

