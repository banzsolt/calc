#ifndef VANZARIVIEW_H
#define VANZARIVIEW_H

#include <QWidget>
#include "VanzariViewListModel.h"

namespace Ui {
class VanzariView;
}

class VanzariView : public QWidget
{
    Q_OBJECT

public:
    explicit VanzariView(QWidget *parent = 0);
    ~VanzariView();

    static VanzariView* instance()
    {
        static QMutex mutex;
        if (!m_Instance)
        {
            mutex.lock();

            if (!m_Instance)
                m_Instance = new VanzariView;

            mutex.unlock();
        }

        return m_Instance;
    }

    VanzariViewListModel* model;
    QList<Reteta*> items;
    QList<int> *chestiiVandute;

    static VanzariView* m_Instance;

private slots:
    void on_vanzariTreeView_clicked(const QModelIndex &index);

public:
    Ui::VanzariView *ui;
};

#endif // VANZARIVIEW_H
