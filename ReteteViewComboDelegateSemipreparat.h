#ifndef RETETEVIEWCOMBODELEGATESEMIPREPARAT_H
#define RETETEVIEWCOMBODELEGATESEMIPREPARAT_H

#include <QItemDelegate>
#include "Backend/ingredient.h"
#include "Backend/Reteta.h"

class ReteteViewComboDelegateSemipreparat : public QItemDelegate
{
        Q_OBJECT
public:
    ReteteViewComboDelegateSemipreparat(QList<Reteta*> itemList, QObject *parent = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    QList<Reteta*> items;

};

#endif // RETETEVIEWCOMBODELEGATESEMIPREPARAT_H
