#ifndef RETETEVIEW_H
#define RETETEVIEW_H

#include <QWidget>
#include "ReteteViewListModel.h"
#include "ReteteViewSemiModel.h"
#include <QMutex>

namespace Ui {
class ReteteView;
}

class ReteteView : public QWidget
{
    Q_OBJECT

public:
    explicit ReteteView(QWidget *parent = 0);
    ~ReteteView();

    static ReteteView* instance()
    {
        static QMutex mutex;
        if (!m_Instance)
        {
            mutex.lock();

            if (!m_Instance)
                m_Instance = new ReteteView;

            mutex.unlock();
        }

        return m_Instance;
    }

    ReteteViewListModel* modelReteteView;
    ReteteViewSemiModel* modelSemipreparatView;

    QList<Ingredient*> items;
    QList<Reteta*> itemsSemipreparate;


    static ReteteView* m_Instance;

    QString currentSelectedReteta;
    int currentSelectedRow;


    int currentIngredienteSelectedRow;
    int currentSemipreparatSelectedRow;

    void contextMenuEvent(QContextMenuEvent *event);


    void                reset();

    Ui::ReteteView *ui;
private slots:
    void on_listaRetete_itemSelectionChanged();
    void on_addReteta_clicked();
    void on_addIngredient_clicked();
    void on_listaRetete_clicked(const QModelIndex &index);
    void on_addSemipreparat_clicked();
    void on_detaliiSemipreparat_clicked(const QModelIndex &index);
    void on_detaliiRetete_clicked(const QModelIndex &index);
    void editReteta();
    void deleteReteta();
    void on_removeIngredient_clicked();
    void on_removeSemipreparat_clicked();
    void on_pretDeVanzare_textChanged(const QString &arg1);
    void on_unitateDeMasura_textChanged(const QString &arg1);
};

#endif // RETETEVIEW_H
