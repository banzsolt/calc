#include "ReteteViewListModel.h"
#include <qpixmap.h>
#include <qicon.h>
#include <qstring.h>
#include <QDebug>
#include "MainWindow.h"

ReteteViewListModel::ReteteViewListModel(QObject* parent)
    : QAbstractListModel(parent)
{
}

ReteteViewListModel::ReteteViewListModel(QList<Ingredient *> ingrediente, QObject* parent)
    : QAbstractListModel(parent)
{
    ingredienteList = ingrediente;
}

ReteteViewListModel::~ReteteViewListModel()
{
}

int ReteteViewListModel::rowCount(const QModelIndex& ) const
{
    return ingredienteList.size();
}

int ReteteViewListModel::columnCount(const QModelIndex &parent) const
{
    return 3;
}

void ReteteViewListModel::setComboData(int row, QString name)
{
    ingredienteList.value(row)->SetName(name);
}

QVariant ReteteViewListModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        if (index.column() == 0)
        {
            //qDebug() << ingredienteList.value(index.row())->GetName();
            return ingredienteList.value(index.row())->GetName();
        }

        if (index.column() == 1)
            return ingredienteList.value(index.row())->GetType();

        if (index.column() == 2)
        {
            if (ReteteView::m_Instance->currentSelectedReteta.length() != 0)
            {
                int i = MainWindow::m_Instance->listaRetete->value(ReteteView::m_Instance->currentSelectedRow)->GetCantitatePtIngredient(index.row());
                return i;
            }
            else
            {
                int i = MainWindow::m_Instance->listaRetete->value(ReteteView::m_Instance->currentSelectedRow)->GetCantitatePtIngredient(index.row());
                return i;
            }
        }
    }

    if(role == Qt::UserRole)
    {

    }

    if(role == Qt::EditRole)
    {
        if (index.column() == 0)
            //return ingredienteList.value(ReteteView::m_Instance->currentIngredienteSelectedRow)->GetName();

        if (index.column() == 1)
            return ingredienteList.value(index.row())->GetType();

        if (index.column() == 2);
    }

    if(role == Qt::ToolTipRole)
    {
        return "Hex code: " + ingredienteList.value(index.row())->GetName();
    }

    return QVariant();
}



QVariant ReteteViewListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
        switch(section)
        {
        case 0:
            return ("Nume materie prima");

        case 2:
            return ("Cantitate");

        case 1:
            return ("Tipul produsului");

        default:
            return QVariant();
        }
    }
    else
    {
        return QString("Color %1").arg(section);
    }
}

bool ReteteViewListModel::insertRows(int position, int rows, const QModelIndex &index)
{
    beginInsertRows(QModelIndex(), position, position + rows - 1);

    Ingredient *newIngredient = new Ingredient("nou", 1000, 0, 0);
    ingredienteList.insert(position, newIngredient);
    MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->AddIngredient(newIngredient, 0);
    MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

    endInsertRows();
    return true;
}

bool ReteteViewListModel::insertIngredient(Ingredient *newIngredient, int position, int rows, const QModelIndex &index)
{
    beginInsertRows(QModelIndex(), position, position + rows - 1);

    if (newIngredient)
        ingredienteList.insert(position, newIngredient);

    endInsertRows();
    return true;
}

bool ReteteViewListModel::removeRows(int position, int rows, const QModelIndex &index)
{
    beginRemoveRows(QModelIndex(), position, position + rows - 1);

    for(int row = 0; row < rows; ++row)
    {
        MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->RemoveIngredient(ingredienteList.at(position));
        ingredienteList.removeAt(position);
    }

    endRemoveRows();
    return true;
}

bool ReteteViewListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{

    if(index.isValid() && role == Qt::EditRole)
    {
        int row = index.row();

        if (index.column() == 0)
        {
            qDebug() << "the name of the ingredient has changed to:" << value.toString();
            //aici facut un delegat
        }
        else if (index.column() == 2)
        {
            MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->SetCantitatePtIngredient(row, int(value.value<int>()));

            MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);


            emit(dataChanged(index, index));
        }

        return true;
    }


    if(index.isValid() && role == Qt::UserRole)
    {
        int row = index.row();

        QString name = value.toString();

        Ingredient *newIngredient = new Ingredient(name,
                                   ingredienteList.value(row)->GetUnitateDeMasura(),
                                   ingredienteList.value(row)->GetPret(),
                                   -1,
                                   "-");

        ingredienteList.replace(row, newIngredient);

        emit(dataChanged(index, index));

        return true;
    }


    return false;
}

Qt::ItemFlags ReteteViewListModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}
