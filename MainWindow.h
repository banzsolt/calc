#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ReteteView.h"
#include "IngredienteView.h"
#include "VanzariView.h"
#include "RaportView.h"
#include <QMutex>
#include "Backend/Ingredient.h"
#include "Backend/reteta.h"
#include "Backend/listingredient.h"
#include "Backend/ReadFromFile.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static MainWindow* instance()
    {
        static QMutex mutex;
        if (!m_Instance)
        {
            mutex.lock();

            if (!m_Instance)
                m_Instance = new MainWindow;

            mutex.unlock();
        }

        return m_Instance;
    }

    ReteteView          *reteteView;
    IngredienteView     *ingredienteView;
    VanzariView         *vanzariView;
    RaportView          *raportView;

    QList<Ingredient*>  *listaIngred;
    QList<Reteta*>      *listaRetete;

    ReadFromFile        *readFromFile;

    static MainWindow* m_Instance;

    Ui::MainWindow *ui;
public slots:
    void on_actionRetete_triggered();
    void on_actionIngrediente_triggered();
    void on_actionVanzari_triggered();
    void on_actionRaport_triggered();
};

#endif // MAINWINDOW_H
