#include "ReteteViewSemiModel.h"
#include <qpixmap.h>
#include <qicon.h>
#include <qstring.h>
#include <QDebug>
#include "MainWindow.h"

ReteteViewSemiModel::ReteteViewSemiModel(QObject* parent)
    : QAbstractListModel(parent)
{
}

ReteteViewSemiModel::ReteteViewSemiModel(QList<Reteta *> ingrediente, QObject* parent)
    : QAbstractListModel(parent)
{
    semipreparateList = ingrediente;
}

ReteteViewSemiModel::~ReteteViewSemiModel()
{
}

int ReteteViewSemiModel::rowCount(const QModelIndex& ) const
{
    return semipreparateList.size();
}

int ReteteViewSemiModel::columnCount(const QModelIndex &parent) const
{
    return 2;
}

void ReteteViewSemiModel::setComboData(int row, QString name)
{
    semipreparateList.value(row)->name = name;
}

QVariant ReteteViewSemiModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        //return ingredienteList.at(index.row());
        if (index.column() == 0)
        {
            return semipreparateList.value(index.row())->name;
        }

        if (index.column() == 1)
        {

            //Reteta *newReteta = new Reteta();
            //newReteta = MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentIngredienteSelectedRow);
            if (ReteteView::m_Instance->currentSelectedReteta.length() != 0)
            {
                qDebug() << index.row();
                int i = MainWindow::m_Instance->listaRetete->value(ReteteView::m_Instance->currentSelectedRow)->GetCantitatePtSemipreparat(index.row());
                qDebug() << i;
                return i;
            }
            else
            {
                int i = MainWindow::m_Instance->listaRetete->value(ReteteView::m_Instance->currentSelectedRow)->GetCantitatePtSemipreparat(index.row());
                return i;
            }
        }
    }

    if(role == Qt::UserRole)
    {

    }

    if(role == Qt::EditRole)
    {
        if (index.column() == 0)
            return semipreparateList.value(index.row())->name;

        if (index.column() == 1)

            return "-";;

    }

    if(role == Qt::ToolTipRole)
    {
        return "Hex code: " + semipreparateList.value(index.row())->name;
    }

    return QVariant();
}



QVariant ReteteViewSemiModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
        switch(section)
        {
        case 0:
            return ("Nume materie prima");

        case 1:
            return ("Cantitate");

        default:
            return QVariant();
        }
    }
    else
    {
        return QString("Color %1").arg(section);
    }
}

bool ReteteViewSemiModel::insertRows(int position, int rows, const QModelIndex &index)
{
    beginInsertRows(QModelIndex(), position, position + rows - 1);

    Reteta *newReteta = new Reteta();
    semipreparateList.insert(position, newReteta);
    MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->AddSemipreparat(newReteta, 0);

//    int l = 0;
//    for(int row = 0; row < rows; row++)
//    {
//        if (l == 0)
//        {
//            if (!(MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->semipreparate.size() == MainWindow::m_Instance->listaRetete->size()))
//            {
//                for (int i = 0; i < MainWindow::m_Instance->listaRetete->count() ; ++i)
//                {
//                    QMap<Reteta*, int>::iterator it = MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->semipreparate.begin();
//                    for (int j = 0; j < MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->semipreparate.count(); ++j)
//                    {
//                        qDebug() << "it.key este " << it.key()->name;
//                        if (!(it.key()->name == MainWindow::m_Instance->listaRetete->at(j)->name))
//                        {
//                            qDebug() << j;
//                            l = 1;
//                            Reteta *newReteta = MainWindow::m_Instance->listaRetete->at(j);
//                            semipreparateList.insert(position, newReteta);
//                            MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->AddSemipreparat(newReteta, 0);
//                            break;
//                        }
//                        it++;
//                    }
//                    if (l ==1)
//                    {
//                        break;
//                    }
//                }
//            }
//        }
//        else
//        {
//            break;
//        }
//    }

    endInsertRows();
    return true;
}

bool ReteteViewSemiModel::removeRows(int position, int rows, const QModelIndex &index)
{
    beginRemoveRows(QModelIndex(), position, position + rows - 1);

    for(int row = 0; row < rows; ++row)
    {
        MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->RemoveSemipreparat(semipreparateList.at(position));
        semipreparateList.removeAt(position);
    }

    endRemoveRows();
    return true;


}

bool ReteteViewSemiModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.isValid() && role == Qt::EditRole)
    {
        int row = index.row();

        if (index.column() == 0)
        {
//            QString name = QString(value.value<QString>());

//            ingredienteList.value(row).SetName(name);
//            ingredienteList.replace(row, ingredienteList.value(row));
//            emit(dataChanged(index, index));
        }
        else if (index.column() == 1)
        {
            //qDebug() << semipreparateList.at(row)->name;
            //qDebug() << MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->ingrediente.count();
            //if (ReteteView::m_Instance->currentSelectedReteta.length() != 0)
             //{
            //qDebug() << "La aceasta linie lucrez:"<<row;
            //qDebug() << ReteteView::m_Instance->currentIngredienteSelectedRow;
                //qDebug() << MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentIngredienteSelectedRow)->ingrediente.count();
            //qDebug() << "fLa randul" << ReteteView::m_Instance->currentSemipreparatSelectedRow;
            //qDebug() << int(value.value<int>());
            //qDebug() << MainWindow::m_Instance->listaRetete->at(1)->GetCantitatePtSemipreparat(0);
                 MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->SetCantitatePtSemipreparat(row, int(value.value<int>()));
                 MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);
             //}

            emit(dataChanged(index, index));
        }

        return true;
    }


    if(index.isValid() && role == Qt::UserRole)
    {
//        int row = index.row();

//        QString name = value.toString();

//        Reteta *newReteta = new Reteta(semipreparateList.value(row)->name,
//                                   true,semipreparateList.value(row)->unitateDeMasura,
//                                   0);

//        semipreparateList.replace(row, newReteta);

//        emit(dataChanged(index, index));

//        return true;
    }


    return false;
}

Qt::ItemFlags ReteteViewSemiModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}
