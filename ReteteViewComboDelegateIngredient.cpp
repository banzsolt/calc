#include "ReteteViewComboDelegateIngredient.h"
#include "MainWindow.h"
#include <QComboBox>
#include "ReteteView.h"
#include "ui_ReteteView.h"
#include "Backend/reteta.h"

ReteteViewComboDelegateIngredient::ReteteViewComboDelegateIngredient(QList<Ingredient *> itemList, QObject *parent)
{
        items = itemList;
}

QWidget *ReteteViewComboDelegateIngredient::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QComboBox *editor = new QComboBox(parent);
    QStringList list ;

    qDebug() << "index.row= "<<index.row();

    for (int i = 0; i < MainWindow::m_Instance->listaIngred->count(); ++i)
    {

        Ingredient newIngredient;
        newIngredient = *MainWindow::m_Instance->listaIngred->at(i);
        int count = 0;
        QMap<Ingredient*, int>::iterator it = MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->ingrediente.begin();
        for (int j = 0; j < MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->ingrediente.count(); ++j)
        {
            if(it.key()->name == newIngredient.name)
            {
                break;
            }
            else
            {
                count++;
            }
            qDebug() << count;
            if (count == MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->ingrediente.count())
            {
                editor->addItem(newIngredient.name);
            }
            it++;
        }
    }

    return editor;
}

void ReteteViewComboDelegateIngredient::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString value = index.model()->data(index, Qt::DisplayRole).toString();
    QComboBox *comboBox = static_cast<QComboBox*>(editor);

    for (int i = 0; i<MainWindow::m_Instance->listaIngred->count() ; ++i)
    {
        if (MainWindow::m_Instance->listaIngred->at(i)->name == comboBox->currentText())
        {
            //beginInsertRows(index.parent(), rowCount(index.parent()), rowCount(index.parent()));

            Ingredient *theIngredient = new Ingredient();
            theIngredient = MainWindow::m_Instance->listaIngred->at(i);

            QString inWhat = MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->ReturnIngredient(index.row());

            MainWindow::m_Instance->listaRetete->value(ReteteView::m_Instance->currentSelectedRow)->ChangeIngredient(inWhat, theIngredient, MainWindow::m_Instance->listaIngred);
            MainWindow::m_Instance->readFromFile->WriteListReteteChange(MainWindow::m_Instance->listaRetete);

            //ReteteView::reset();

            //ReteteView::m_Instance->ui->detaliiRetete->;
        }
    }
}

void ReteteViewComboDelegateIngredient::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    //model->beginInsertRows(index, 0, MainWindow::m_Instance->listaRetete->at(ReteteView::m_Instance->currentSelectedRow)->ingrediente.count());

        QComboBox *comboBox = static_cast<QComboBox*>(editor);
        QString value = comboBox->currentText();
        model->setData(index, value, Qt::UserRole);

    //model->endInsertRows();

    //ReteteView::m_Instance->ui->detaliiRetete->update(index);

//    ReteteView::m_Instance->ui->detaliiRetete->setModel(NULL);
//    ReteteView::m_Instance->ui->detaliiRetete->setModel(model);
    qDebug() << "reset 2 called";
}

void ReteteViewComboDelegateIngredient::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}
